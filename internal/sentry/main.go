package sentry

import (
	"fmt"

	downstream "github.com/atlassian/go-sentry-api"
	"github.com/pkg/errors"
)

type Client struct {
	downstream *downstream.Client
	token      string
	endpoint   string

	teamName *string
	teamSlug *string
	org      *string
}

func NewClient(token string, endpoint string) *Client {
	return &Client{
		token:    token,
		endpoint: fmt.Sprintf("%s/api/0/", endpoint),
	}
}

func (c Client) WithOrganization(slug string) *Client {
	c.org = &slug
	return &c
}

func (c Client) WithTeam(name *string, slug *string) *Client {
	c.teamName = name
	c.teamSlug = slug
	return &c
}

func (c *Client) EnsureTeam(name string) (slug *string, err error) {
	if c.downstream == nil {
		c.downstream, err = downstream.NewClient(c.token, &c.endpoint, nil)
		if err != nil {
			return nil, errors.Wrap(err, "failed to init Sentry client")
		}
	}
	teams, err := c.downstream.GetOrganizationTeams(downstream.Organization{
		Slug: c.org,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get teams")
	}

	for _, team := range teams {
		if team.Name == name {
			return team.Slug, nil
		}
	}

	team, err := c.downstream.CreateTeam(downstream.Organization{
		Slug: c.org,
	}, name, nil)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create team")
	}

	return team.Slug, nil
}

func (c *Client) EnsureProject(slug string) (err error) {
	if c.downstream == nil {
		c.downstream, err = downstream.NewClient(c.token, &c.endpoint, nil)
		if err != nil {
			return errors.Wrap(err, "failed to init Sentry client")
		}
	}

	projects, err := c.downstream.GetTeamProjects(downstream.Organization{Slug: c.org}, downstream.Team{Slug: c.teamSlug})
	if err != nil {
		return errors.Wrap(err, "failed to get team projects")
	}

	for _, project := range projects {
		if project.Name == slug {
			return nil
		}
	}

	_, err = c.downstream.CreateProject(
		downstream.Organization{Slug: c.org},
		downstream.Team{Slug: c.teamSlug},
		slug, nil,
	)

	if err != nil {
		return errors.Wrap(err, "failed to create project")
	}

	return nil
}

func (c *Client) ClientKey(slug string, label string) (_ Key, err error) {
	if c.downstream == nil {
		c.downstream, err = downstream.NewClient(c.token, &c.endpoint, nil)
		if err != nil {
			return Key{}, errors.Wrap(err, "failed to init Sentry client")
		}
	}

	projects, err := c.downstream.GetTeamProjects(downstream.Organization{Slug: c.org}, downstream.Team{Slug: c.teamSlug})
	if err != nil {
		return Key{}, errors.Wrap(err, "failed to get team projects")
	}

	for _, project := range projects {
		if project.Name == slug {
			keys, err := c.downstream.GetClientKeys(downstream.Organization{Slug: c.org}, project)
			if err != nil {
				return Key{}, errors.Wrap(err, "failed to get keys")
			}
			for _, key := range keys {
				if key.Label == label {
					return Key{
						Public: key.DSN.Public,
						Secret: key.DSN.Secret,
					}, nil
				}
			}
			key, err := c.downstream.CreateClientKey(downstream.Organization{Slug: c.org}, project, label)
			if err != nil {
				return Key{}, errors.Wrap(err, "failed to create key")
			}

			return Key{
				Public: key.DSN.Public,
				Secret: key.DSN.Secret,
			}, nil
		}
	}
	return Key{}, errors.New("yo yoba")
}

type Key struct {
	Public string
	Secret string
}
