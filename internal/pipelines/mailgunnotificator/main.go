package mailgunnotificator

import (
	"bytes"
	"io"

	"github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Pipeline struct {
	Image   string
	Mailgun struct {
		ApiKey string `fig:"apikey"`
		Domain string `fig:"domain"`
		From   string `fig:"from"`
	}
	Sentry templates.SentryConfig `fig:"sentry"`
}

func (p Pipeline) Name() string {
	return "mailgunnotificator"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("mailgunnotificator", "."), "mailgunnotificator.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	if err := ctx.Get("mailgunnotificator", &p); err != nil {
		return nil, err
	}

	var result []io.Reader
	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		MailgunNotificator Pipeline
	}{
		ctx.Common(),
		ns,
		p,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)
	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if err := ctx.Get("mailgunnotificator", &p); err != nil {
		return nil, errors.Wrap(err, "failed to get env")
	}

	return p, nil
}
