package namespace

import (
	"bytes"
	"io"

	"github.com/gosimple/slug"

	"github.com/gobuffalo/packr/v2"

	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	Namespace        string
	CpuPodLimit      string `fig:"cpupodlimit" yaml:",omitempty"`
	MemoryPodLimit   string `fig:"memorypodlimit" yaml:",omitempty"`
	CpuPodRequest    string `fig:"cpupodrequest" yaml:",omitempty"`
	MemoryPodRequest string `fig:"memorypodrequest" yaml:",omitempty"`
}

type Pipeline struct {
}

func (p Pipeline) Name() string {
	return "namespace"
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var env Env
	if err := ctx.Get("namespace", &env); err != nil {
		return nil, err
	}

	common := ctx.Common()

	if env.Namespace == "" {
		env.Namespace = slug.Make(common.Name)
	}

	return env, nil
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("namespace", "."), "namespace.yaml")

	env := Env{
		CpuPodLimit:      "0.5",
		MemoryPodLimit:   "500Mi",
		CpuPodRequest:    "0.01",
		MemoryPodRequest: "50Mi",
	}
	if err := ctx.Get("namespace", &env); err != nil {
		return nil, err
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		Env
		RawConfigEnv string
	}{
		ctx.Common(),
		env,
		string(ctx.RawEnvironmentConfig()),
	}

	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	return []io.Reader{&b}, nil
}
