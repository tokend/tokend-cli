package d7notificator

import (
	"bytes"
	"io"

	"github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Pipeline struct {
	Image string
	D7    struct {
		Username string `fig:"username"`
		Password string `fig:"password"`
		Sender   string `fig:"sender"`
	}
	Sentry templates.SentryConfig `fig:"sentry"`
}

func (p Pipeline) Name() string {
	return "d7notificator"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("d7notificator", "."), "d7notificator.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	if err := ctx.Get("d7notificator", &p); err != nil {
		return nil, err
	}

	var result []io.Reader
	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		d7Notificator Pipeline
	}{
		ctx.Common(),
		ns,
		p,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)
	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if err := ctx.Get("d7notificator", &p); err != nil {
		return nil, errors.Wrap(err, "failed to get env")
	}

	return p, nil
}
