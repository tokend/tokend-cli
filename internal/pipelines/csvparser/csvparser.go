package csvparser

import (
	"bytes"
	"io"

	"github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	Image    string
	Database struct {
		URL   string `fig:"url" yaml:",omitempty"`
		Local bool   `fig:"local" yaml:",omitempty"`
	} `fig:"database" yaml:",omitempty"`
	Signer        string
	Source        string
	FileKey       string                 `fig:"filekey" yaml:"filekey"`
	MaxRows       int32                  `fig:"maxrows" yaml:"maxrows"`
	ColsSeparator string                 `fig:"colsseparator" yaml:"colsseparator"`
	Sentry        templates.SentryConfig `fig:"sentry"`
	Cop           templates.CopConfig    `fig:"cop" yaml:",omitempty"`
	Janus         templates.JanusConfig  `fig:"janus" yaml:",omitempty"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "csvparser"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	box := packr.New("csvparser", ".")
	template := templates.MustGet(box, "csvparser.yaml")
	pgtemplate := templates.MustGet(box, "csvparser-pg.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	env := Env{
		MaxRows:       32,
		FileKey:       "data.csv",
		ColsSeparator: ",",
	}
	if err := ctx.Get("csvparser", &env); err != nil {
		return nil, err
	}

	var result []io.Reader
	if env.Database.URL == "" && env.Database.Local {
		env.Database.URL = "postgres://csvparser:csvparser@csvparser-postgres/csvparser?sslmode=disable"
		var a bytes.Buffer
		da := struct {
			namespace.Env
			CsvParser Env
		}{
			ns,
			env,
		}
		err := pgtemplate.Execute(&a, da)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render pg template")
		}

		result = append(result, &a)
	}

	if env.Source == "" {
		kp := keypair.MustParseSeed(env.Signer)
		env.Source = kp.Address()
	}

	if ctx.Common().MustTokenDRelease() >= types.TraefikCopTokenDRelease {
		if err := env.Cop.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate cop config")
		}
		env.Cop.ServicePrefix = "/integrations/csv-parser"
	} else {
		if err := env.Janus.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate janus config")
		}
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		CsvParser Env
	}{
		ctx.Common(),
		ns,
		env,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)

	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var env Env
	if err := ctx.Get("csvparser", &env); err != nil {
		return nil, err
	}

	if env.Database.URL == "" {
		env.Database.Local = true
	}

	if env.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate signer")
		}
		env.Signer = kp.Seed()
	}

	if err := env.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to get Sentry DSN")
	}

	return env, nil
}
