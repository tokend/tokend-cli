package proxypayments

import (
	"bytes"
	"io"

	"gitlab.com/tokend/keypair"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	Image    string
	Database struct {
		URL   string `fig:"url" yaml:",omitempty"`
		Local bool   `fig:"local" yaml:",omitempty"`
	} `fig:"database" yaml:",omitempty"`
	Signer  string `fig:"signer"`
	Account string
	Sentry  templates.SentryConfig `fig:"sentry"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "proxypayments"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	box := packr.New("proxypayments", ".")
	template := templates.MustGet(box, "proxypayments.yaml")
	pgtemplate := templates.MustGet(box, "proxypayments-pg.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var env Env
	if err := ctx.Get("proxypayments", &env); err != nil {
		return nil, err
	}

	var result []io.Reader
	if env.Database.URL == "" && env.Database.Local {
		env.Database.URL = "postgres://proxypayments:proxypayments@proxypayments-postgres/proxypayments?sslmode=disable"
		var a bytes.Buffer
		da := struct {
			namespace.Env
			ProxyPayments Env
		}{
			ns,
			env,
		}
		err := pgtemplate.Execute(&a, da)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render pg template")
		}

		result = append(result, &a)
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		ProxyPayments Env
	}{
		ctx.Common(),
		ns,
		env,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)

	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var env Env
	if err := ctx.Get("proxypayments", &env); err != nil {
		return nil, err
	}

	if env.Database.URL == "" {
		env.Database.Local = true
	}

	if env.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate signer")
		}
		env.Signer = kp.Seed()
		env.Account = kp.Address()
	}

	if env.Account == "" {
		kp, err := keypair.ParseSeed(env.Signer)
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate signer")
		}
		env.Account = kp.Address()
	}

	if err := env.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to get Sentry DSN")
	}

	return env, nil
}
