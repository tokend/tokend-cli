package notificationsrouter

import (
	"bytes"
	"io"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Pipeline struct {
	Image                   string
	Signer                  string
	DefaultLocale           string   `fig:"defaultlocale"`
	DefaultChannelsPriority []string `fig:"defaultchannelspriority"`
	Database                struct {
		URL   string `fig:"url" yaml:",omitempty"`
		Local bool   `fig:"local" yaml:",omitempty"`
	} `fig:"database" yaml:",omitempty"`
	Sentry templates.SentryConfig `fig:"sentry"`
	Cop    templates.CopConfig    `fig:"cop" yaml:",omitempty"`
	Janus  templates.JanusConfig  `fig:"janus" yaml:",omitempty"`
}

func (p Pipeline) Name() string {
	return "notificationsrouter"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("notificationsrouter", "."), "notificationsrouter.yaml")
	pgtemplate := templates.MustGet(packr.New("notificationsrouter-pg", "."), "notificationsrouter-pg.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	if err := ctx.Get("notificationsrouter", &p); err != nil {
		return nil, err
	}

	var result []io.Reader
	if p.Database.URL == "" && p.Database.Local {
		p.Database.URL = "postgres://notificationsrouter:notificationsrouter@notificationsrouter-postgres/notificationsrouter?sslmode=disable"
		var a bytes.Buffer
		da := struct {
			namespace.Env
		}{
			ns,
		}
		err := pgtemplate.Execute(&a, da)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render pg template")
		}

		result = append(result, &a)
	}

	if ctx.Common().MustTokenDRelease() >= types.TraefikCopTokenDRelease {
		if err := p.Cop.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate cop config")
		}
		p.Cop.ServicePrefix = "/integrations/notifications"
	} else {
		if err := p.Janus.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate janus config")
		}
	}

	if len(p.DefaultChannelsPriority) == 0 {
		return nil, errors.New("you must provide defaultchannelspriority list")
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		NotificationsRouter Pipeline
	}{
		ctx.Common(),
		ns,
		p,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)
	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if err := ctx.Get("notificationsrouter", &p); err != nil {
		return nil, errors.Wrap(err, "failed to get env")
	}

	return p, nil
}
