package notificator

import (
	"bytes"
	"io"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Pipeline struct {
	Image    string
	Database struct {
		URL        string `fig:"url" yaml:",omitempty"`
		Local      bool   `fig:"local" yaml:",omitempty"`
		Disposable bool   `fig:"disposable" yaml:",omitempty"`
	} `fig:"database" yaml:",omitempty"`
	Mailgun struct {
		Key    string `fig:"key"`
		Domain string `fig:"domain"`
		From   string `fig:"from"`
	} `fig:"mailgun"`
}

func (p Pipeline) Name() string {
	return "notificator"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("notificator", "."), "notificator.yaml")
	pgtemplate := templates.MustGet(packr.New("notificator-pg", "."), "notificator-pg.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	if err := ctx.Get("notificator", &p); err != nil {
		return nil, err
	}

	var result []io.Reader
	if p.Database.URL == "" && p.Database.Local {
		p.Database.URL = "postgres://notificator:notificator@notificator-postgres/notificator?sslmode=disable"
		var a bytes.Buffer
		da := struct {
			namespace.Env
		}{
			ns,
		}
		err := pgtemplate.Execute(&a, da)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render pg template")
		}

		result = append(result, &a)
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		Notificator Pipeline
	}{
		ctx.Common(),
		ns,
		p,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)
	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if err := ctx.Get("notificator", &p); err != nil {
		return nil, errors.Wrap(err, "failed to get env")
	}

	return p, nil
}
