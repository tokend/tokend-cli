package core

import (
	"bytes"
	"fmt"
	"io"
	"reflect"
	"strings"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/ingress"

	"github.com/gobuffalo/packr/v2"
	"github.com/gosimple/slug"
	"github.com/pkg/errors"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	NodesCount           int      `fig:"nodescount"`
	NetworkPassphrase    string   `fig:"networkpassphrase"`
	MasterAccountID      string   `fig:"masteraccountid"`
	DefaultImage         string   `fig:"defaultimage,required"`
	DefaultHistoryRegion string   `fig:"defaulthistoryregion"`
	TxInternalError      []string `fig:"txinternalerror" yaml:",omitempty"`
	AuditNodeNeeded      bool     `fig:"auditnodeneeded" yaml:",omitempty"`
	ThresholdPercent     int      `fig:"thresholdpercent" yaml:",omitempty"`
	// ProbeBuckets ensure history bucket existense before node startup.
	// Conflicts with Disposable FIXME add validation for that
	DisableProbeBuckets bool                   `fig:"disableprobebuckets" yaml:",omitempty"`
	BaseExchangeName    string                 `fig:"baseexchangename" yaml:",omitempty"`
	Nodes               []NodeEnv              `fig:"nodes"`
	AuditNode           *NodeEnv               `fig:"auditnode" yaml:"auditnode,omitempty"`
	DefaultSentry       templates.SentryConfig `fig:"defaultsentry"`
}

var NodeHook = figure.Hooks{
	"*templates.SentryConfig": func(value interface{}) (reflect.Value, error) {
		var c templates.SentryConfig
		if err := figure.Out(&c).From(cast.ToStringMap(value)).Please(); err != nil {
			return reflect.Value{}, errors.Wrap(err, "failed to figure out")
		}
		return reflect.ValueOf(c), nil
	},
	"[]core.NodeEnv": func(value interface{}) (reflect.Value, error) {
		slice, err := cast.ToSliceE(value)
		if err != nil {
			return reflect.Value{}, err
		}
		r := []NodeEnv{}
		for _, n := range slice {
			a, err := cast.ToStringMapE(n)
			if err != nil {
				return reflect.Value{}, err
			}
			var node NodeEnv
			if err := figure.Out(&node).From(a).Please(); err != nil {
				return reflect.Value{}, err
			}
			r = append(r, node)
		}
		return reflect.ValueOf(r), nil
	},
	"*core.NodeEnv": func(value interface{}) (reflect.Value, error) {
		switch value.(type) {
		case string, map[interface{}]interface{}:
			a, err := cast.ToStringMapE(value)
			if err != nil {
				return reflect.Value{}, err
			}
			var node NodeEnv
			if err := figure.Out(&node).From(a).Please(); err != nil {
				return reflect.Value{}, err
			}
			return reflect.ValueOf(&node), nil
		case nil:
			return reflect.ValueOf(nil), nil
		default:
			return reflect.Value{}, fmt.Errorf("unsupported conversion from %T", value)
		}
	},
}

type NodeEnv struct {
	Seed          string
	Name          string
	HistoryRegion string `fig:"historyregion" yaml:"historyregion,omitempty"`
	HistoryBucket string `fig:"historybucket"`
	Image         string `yaml:"image,omitempty"`
	Unsafe        bool   `yaml:",omitempty"`
	// Disposable node does not store ledger or history persistently.
	// Should be set explicitly after environment generation
	Disposable bool `fig:"disposable" yaml:",omitempty"`
	// FIXME: at the time of writing figure shits itself on ptr types with nil values
	Sentry      *templates.SentryConfig `fig:"-" yaml:"-,omitempty"`
	Public      bool                    `yaml:",omitempty"`
	Centralized bool                    `yaml:",omitempty"`
	CloseTime   uint32                  `fig:"closetime" yaml:",omitempty"`
	Limits      templates.LimitsConfig  `fig:"limits" yaml:",omitempty"`
}

type Pipeline struct {
}

func (p Pipeline) Name() string {
	return "core"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("core", "."), "core.yaml")

	var result []io.Reader

	var env Env
	if err := ctx.Get("core", &env, NodeHook); err != nil {
		return nil, err
	}

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	if env.BaseExchangeName == "" {
		env.BaseExchangeName = "TokenD"
	}

	if env.ThresholdPercent == 0 {
		env.ThresholdPercent = ((len(env.Nodes)*2-1)/3)*100/len(env.Nodes) + 1
	}

	// FIXME mutation
	for idx := range env.Nodes {
		if env.Nodes[idx].Image == "" {
			env.Nodes[idx].Image = env.DefaultImage
		}

		if env.Nodes[idx].HistoryRegion == "" {
			env.Nodes[idx].HistoryRegion = env.DefaultHistoryRegion
		}

		if env.NodesCount < 4 && !env.Nodes[idx].Unsafe {
			panic("Cannot generate deployment with safe quorum " +
				"having less than 4 nodes. " +
				"Please set `unsafe: true` for all nodes in deployment.\n" +
				"e.g.:\n" +
				"  nodes:\n" +
				"  - historybucket: example-core-elegant-hodgkin\n" +
				"    name: core-elegant-hodgkin\n" +
				"    seed: SBNN4OITX7DH5EL4XRTG37XKZ6V2BOG4XBRXUVM4DZEAQ52ZDLNUDOYC\n" +
				"    unsafe: true")
		}
	}

	for _, node := range env.Nodes {
		if node.Sentry == nil {
			node.Sentry = &env.DefaultSentry
		}

		var b bytes.Buffer
		d := struct {
			types.Common
			namespace.Env
			Node NodeEnv
			Core Env
		}{
			ctx.Common(),
			ns,
			node,
			env,
		}
		err := template.Execute(&b, d)

		if err != nil {
			return nil, errors.Wrap(err, "failed to render template")
		}

		result = append(result, &b)
	}

	publicNodePresent := false
	for _, node := range env.Nodes {
		if node.Public {
			publicNodePresent = true
			break
		}
	}

	if env.AuditNodeNeeded {
		if env.AuditNode == nil {
			env.AuditNode = &NodeEnv{}
		}

		node := env.AuditNode

		node.Public = true
		if node.Sentry == nil {
			node.Sentry = &env.DefaultSentry
		}

		if node.Image == "" {
			node.Image = env.DefaultImage
		}

		if node.HistoryRegion == "" {
			node.HistoryRegion = env.DefaultHistoryRegion
		}
		if node.Limits.Resources.Limits.CPU == "" {
			node.Limits.Resources.Limits.CPU = "1"
		}

		if node.Limits.Resources.Limits.Memory == "" {
			node.Limits.Resources.Limits.Memory = "1000Mi"
		}

		if node.Limits.Resources.Requests.CPU == "" {
			node.Limits.Resources.Requests.CPU = "0.1"
		}

		if node.Limits.Resources.Requests.Memory == "" {
			node.Limits.Resources.Requests.Memory = "50Mi"
		}

		var ingressEnv ingress.Env
		if err := ctx.Get("ingress", &ingressEnv); err != nil {
			return nil, errors.Wrap(err, "failed to get ingress config")
		}

		b, err := renderPublicNode(ns, env, ingressEnv)

		if err != nil {
			return nil, errors.Wrap(err, "failed to render audit node template")
		}

		result = append(result, b)
	}

	if publicNodePresent {
		var ingressEnv ingress.Env
		if err := ctx.Get("ingress", &ingressEnv); err != nil {
			return nil, errors.Wrap(err, "failed to get ingress config")
		}

		bucketsConf, err := renderBucketsProxy(ns, env, ingressEnv)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render buckets proxy")
		}
		result = append(result, bucketsConf)
	}

	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	env := Env{
		NodesCount:           4,
		DefaultHistoryRegion: "eu-west-1",
	}
	if err := ctx.Get("core", &env, NodeHook); err != nil {
		return nil, errors.Wrap(err, "failed to get core config")
	}

	if len(env.Nodes) != 0 && len(env.Nodes) != env.NodesCount {
		panic("core cluster resize is not implemented")
	}

	if env.NodesCount < 4 {
		fmt.Println("NOTE: As there are less than 4 core nodes," +
			" they will run in unsafe quorum mode")
	}

	if len(env.Nodes) == 0 {
		env.Nodes = make([]NodeEnv, env.NodesCount)
	}

	if env.NetworkPassphrase == "" {
		env.NetworkPassphrase = fmt.Sprintf("%s Network!", ctx.Common().Name)
	}

	if err := env.DefaultSentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to get Sentry DSN")
	}

	for idx := range env.Nodes {
		node := &env.Nodes[idx]
		if node.Name == "" {
			node.Name = fmt.Sprintf("core-%s", ctx.RandomString())
		}

		kp, _ := keypair.Random()
		if node.Seed == "" {
			node.Seed = kp.Seed()
		}

		if node.HistoryBucket == "" {
			node.HistoryBucket = strings.ToLower(
				fmt.Sprintf("%s-%s", slug.Make(ctx.Common().Name), node.Name),
			)
		}

		if env.NodesCount < 4 {
			node.Unsafe = true
		}
	}

	if env.AuditNodeNeeded {
		env.AuditNode = &NodeEnv{}
		node := env.AuditNode
		if node.Name == "" {
			node.Name = "audit-node"
		}

		kp, _ := keypair.Random()
		if node.Seed == "" {
			node.Seed = kp.Seed()
		}

		if node.HistoryBucket == "" {
			node.HistoryBucket = strings.ToLower(
				fmt.Sprintf("%s-%s", slug.Make(ctx.Common().Name), node.Name),
			)
		}

		if env.NodesCount < 4 {
			node.Unsafe = true
		}
	}

	if env.BaseExchangeName == "" {
		env.BaseExchangeName = ctx.Common().Name
	}

	return env, nil
}

func renderBucketsProxy(ns namespace.Env, core Env, ingressEnv ingress.Env) (*bytes.Buffer, error) {
	template := templates.MustGet(packr.New("bucketsproxy", "."), "buckets.yaml")

	var b bytes.Buffer
	d := struct {
		namespace.Env
		Core    Env
		Ingress ingress.Env
	}{
		ns,
		core,
		ingressEnv,
	}
	err := template.Execute(&b, d)

	if err != nil {
		return nil, err
	}
	return &b, nil
}

func renderPublicNode(ns namespace.Env, core Env, ingressEnv ingress.Env) (*bytes.Buffer, error) {
	template := templates.MustGet(packr.New("audit", "."), "audit.yaml")

	var b bytes.Buffer
	d := struct {
		namespace.Env
		Core    Env
		Node    NodeEnv
		Ingress ingress.Env
	}{
		ns,
		core,
		*core.AuditNode,
		ingressEnv,
	}
	err := template.Execute(&b, d)

	if err != nil {
		return nil, err
	}
	return &b, nil
}
