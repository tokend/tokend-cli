package traefik

import (
	"bytes"

	packr "github.com/gobuffalo/packr/v2"

	"io"

	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	Image           string
	EnableDashboard bool `fig:"enabledashboard"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "traefik"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	if ctx.Common().MustTokenDRelease() < types.TraefikCopTokenDRelease {
		return nil, nil
	}
	template := templates.MustGet(packr.New("traefik", "."), "traefik.yaml")

	var nsEnv namespace.Env
	if err := ctx.Get("namespace", &nsEnv); err != nil {
		return nil, errors.Wrap(err, "failed to get namespace config")
	}

	var env Env
	if err := ctx.Get("traefik", &env); err != nil {
		return nil, errors.Wrap(err, "failed to get traefik config")
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		Traefik Env
	}{
		ctx.Common(),
		nsEnv,
		env,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	return []io.Reader{&b}, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if ctx.Common().MustTokenDRelease() < types.TraefikCopTokenDRelease {
		return nil, nil
	}
	var env Env
	if err := ctx.Get("traefik", &env); err != nil {
		return nil, errors.Wrap(err, "failed to get traefik config")
	}

	return env, nil
}
