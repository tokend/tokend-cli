package api

import (
	"bytes"
	"fmt"
	"io"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/ingress"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	Version  int `fig:"version"`
	Image    string
	Database struct {
		URL            string `fig:"url" yaml:",omitempty"`
		Local          bool   `fig:"local" yaml:",omitempty"`
		Disposable     bool   `fig:"disposable" yaml:",omitempty"`
		MaxConnections int    `fig:"maxconnections" yaml:",omitempty"`
	} `fig:"database" yaml:",omitempty"`
	Signer              string `fig:"signer"`
	RPS                 int    `fig:"rps" yaml:",omitempty"`
	TfaExpirationPeriod string `fig:"tfaexpirationperiod" yaml:",omitempty"`
	Storage             struct {
		Region    string `fig:"region"`
		AccessKey string `fig:"accesskey"`
		SecretKey string `fig:"secretkey"`
		Bucket    string `fig:"bucket"`
	} `fig:"storage"`
	Wallets struct {
		DisableConfirm bool `fig:"disableconfirm"`
	} `fig:"wallets"`
	EnableEmailConfirmations bool   `fig:"enableemailconfirmations"`
	WalletsCleanerPeriod     string `fig:"walletscleanerperiod"`
	KeyValues                struct {
		LicenseAdmin string `fig:"licenseadmin"`
	} `fig:"keyvalues"`
	Notificator struct {
		// This can be enabled manualy, or will be enabled if EnableEmailConfirmations is true
		Required     bool   `yaml:",omitempty"`
		Endpoint     string `fig:"endpoint" yaml:",omitempty"`
		ClientRouter string `fig:"client_router" yaml:",omitempty"`
	} `fig:"notificator" yaml:",omitempty"`
	DisableProbeBuckets bool                   `fig:"disableprobebuckets"`
	Sentry              templates.SentryConfig `fig:"sentry"`
	Cop                 templates.CopConfig    `fig:"cop" yaml:",omitempty"`
	Janus               templates.JanusConfig  `fig:"janus" yaml:",omitempty"`
	Telegram            struct {
		Enabled  bool   `yaml:",omitempty"`
		BotURL   string `fig:"boturl" yaml:",omitempty"`
		BotToken string `fig:"bottoken" yaml:",omitempty"`
	}
	Twilio struct {
		URL        string `yaml:",omitempty"`
		AccountSID string `fig:"accountsid" yaml:",omitempty"`
		AuthToken  string `fig:"authtoken" yaml:",omitempty"`
		Sender     string `yaml:",omitempty"`
	} `fig:"twilio"`
	TotpIssuer string `fig:"totp_issuer"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "api"
}

func (p Pipeline) CurrentVersion() string {
	// 2:
	// SentryDSN -> Sentry.DSN
	// Add Sentry.Disabled
	return "2"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	box := packr.New("api", ".")
	template := templates.MustGet(box, "api.yaml")
	pgtemplate := templates.MustGet(box, "api-pg.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	env := Env{
		WalletsCleanerPeriod: "48h",
		TfaExpirationPeriod:  "24h",
	}
	if err := ctx.Get("api", &env); err != nil {
		return nil, err
	}
	if env.KeyValues.LicenseAdmin == "" {
		env.KeyValues.LicenseAdmin = "license_admin_signer_role"
	}

	env.Notificator.Required = env.Notificator.Required || env.EnableEmailConfirmations
	if env.Notificator.Endpoint == "" {
		env.Notificator.Endpoint = "http://notificator"
	}

	var ingressEnv ingress.Env
	if err := ctx.Get("ingress", &ingressEnv); err != nil {
		return nil, err
	}

	var result []io.Reader
	if env.Database.URL == "" && env.Database.Local {
		env.Database.URL = "postgres://identity:identity@identity-postgres/identity?sslmode=disable"
		var a bytes.Buffer
		da := struct {
			namespace.Env
			API Env
		}{
			ns,
			env,
		}
		err := pgtemplate.Execute(&a, da)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render pg template")
		}

		result = append(result, &a)
	}

	if ctx.Common().MustTokenDRelease() >= types.TraefikCopTokenDRelease {
		if err := env.Cop.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate cop config")
		}
	} else {
		if err := env.Janus.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate janus config")
		}
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		API     Env
		Ingress ingress.Env
	}{
		ctx.Common(),
		ns,
		env,
		ingressEnv,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)

	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	env := Env{
		WalletsCleanerPeriod: "48h",
		TfaExpirationPeriod:  "24h",
	}
	if err := ctx.Get("api", &env); err != nil {
		return nil, err
	}

	if env.Database.URL == "" {
		env.Database.Local = true
	}

	if env.Storage.Bucket == "" {
		env.Storage.Bucket = fmt.Sprintf("%s-identity-storage-%s", ns.Namespace, ctx.RandomString())
	}

	if env.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate signer")
		}
		env.Signer = kp.Seed()
	}

	if err := env.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to get Sentry DSN")
	}

	return env, nil
}
