package redis

import (
	"bytes"
	"io"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/types"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/templates"
)

type Env struct {
	Image string
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "redis"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("redis", "."), "redis.yaml")

	var nsEnv namespace.Env
	if err := ctx.Get("namespace", &nsEnv); err != nil {
		return nil, errors.Wrap(err, "failed to get namespace config")
	}

	var env Env
	if err := ctx.Get("redis", &env); err != nil {
		return nil, errors.Wrap(err, "failed to get redis config")
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		Redis Env
	}{
		ctx.Common(),
		nsEnv,
		env,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	return []io.Reader{&b}, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var env Env
	if err := ctx.Get("redis", &env); err != nil {
		return nil, errors.Wrap(err, "failed to get redis config")
	}

	return env, nil
}
