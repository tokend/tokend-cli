package invites

import (
	"bytes"
	"io"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	Image    string
	Database struct {
		URL   string `fig:"url" yaml:",omitempty"`
		Local bool   `fig:"local" yaml:",omitempty"`
	} `fig:"database" yaml:",omitempty"`
	Signer string
	// TODO populate sentry
	Sentry templates.SentryConfig `fig:"sentry"`
	Cop    templates.CopConfig    `fig:"cop" yaml:",omitempty"`
	Janus  templates.JanusConfig  `fig:"janus" yaml:",omitempty"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "invites"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	box := packr.New("invites", ".")
	template := templates.MustGet(box, "invites.yaml")
	pgtemplate := templates.MustGet(box, "invites-pg.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var env Env
	if err := ctx.Get("invites", &env); err != nil {
		return nil, err
	}

	var result []io.Reader

	if env.Database.URL == "" && env.Database.Local {
		env.Database.URL = "postgres://invites:invites@invites-postgres/invites?sslmode=disable"
		var a bytes.Buffer
		da := struct {
			namespace.Env
		}{
			ns,
		}
		err := pgtemplate.Execute(&a, da)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render pg template")
		}

		result = append(result, &a)
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		Invites Env
	}{
		ctx.Common(),
		ns,
		env,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)

	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var env Env
	if err := ctx.Get("invites", &env); err != nil {
		return nil, err
	}

	if ctx.Common().MustTokenDRelease() >= types.TraefikCopTokenDRelease {
		if err := env.Cop.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate cop config")
		}
	} else {
		if err := env.Janus.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate janus config")
		}
	}

	return env, nil
}
