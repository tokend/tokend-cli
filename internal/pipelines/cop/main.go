package cop

import (
	"bytes"
	"io"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/types"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/templates"
)

type Env struct {
	Image  string
	Sentry templates.SentryConfig `fig:"sentry"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "cop"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	if ctx.Common().MustTokenDRelease() < types.TraefikCopTokenDRelease {
		return nil, nil
	}
	template := templates.MustGet(packr.New("cop", "."), "cop.yaml")

	var nsEnv namespace.Env
	if err := ctx.Get("namespace", &nsEnv); err != nil {
		return nil, errors.Wrap(err, "failed to get namespace config")
	}

	var env Env
	if err := ctx.Get("cop", &env); err != nil {
		return nil, errors.Wrap(err, "failed to get cop config")
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		Cop Env
	}{
		ctx.Common(),
		nsEnv,
		env,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	return []io.Reader{&b}, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if ctx.Common().MustTokenDRelease() < types.TraefikCopTokenDRelease {
		return nil, nil
	}
	var env Env
	if err := ctx.Get("cop", &env); err != nil {
		return nil, errors.Wrap(err, "failed to get cop config")
	}

	if err := env.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to get Sentry DSN")
	}

	return env, nil
}
