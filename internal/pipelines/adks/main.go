package adks

import (
	"bytes"
	"io"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	Image    string
	Database struct {
		URL        string `fig:"url" yaml:",omitempty"`
		Local      bool   `fig:"local" yaml:",omitempty"`
		Disposable bool   `fig:"disposable" yaml:",omitempty"`
	} `fig:"database" yaml:",omitempty"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "adks"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	box := packr.New("adks", ".")
	template := templates.MustGet(box, "adks.yaml")
	pgtemplate := templates.MustGet(box, "adks-pg.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var env Env
	if err := ctx.Get("adks", &env); err != nil {
		return nil, err
	}

	var result []io.Reader

	if env.Database.URL == "" && env.Database.Local {
		env.Database.URL = "postgres://adks:adks@adks-postgres/adks?sslmode=disable"
		var a bytes.Buffer
		da := struct {
			namespace.Env
		}{
			ns,
		}
		err := pgtemplate.Execute(&a, da)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render pg template")
		}

		result = append(result, &a)
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		ADKS Env
	}{
		ctx.Common(),
		ns,
		env,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)

	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var env Env
	if err := ctx.Get("adks", &env); err != nil {
		return nil, err
	}

	return env, nil
}
