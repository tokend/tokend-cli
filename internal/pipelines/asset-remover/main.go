package assetremover

import (
	"bytes"
	"io"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Pipeline struct {
	Image     string
	Signer    string
	Address   string                 `yaml:",omitempty"`
	Frequency string                 `yaml:",omitempty"`
	Sentry    templates.SentryConfig `fig:"sentry"`
}

func (p Pipeline) Name() string {
	return "assetremover"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("assetremover", "."), "assetremover.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	p.Frequency = "5m"
	if err := ctx.Get("assetremover", &p); err != nil {
		return nil, err
	}

	if p.Address == "" {
		kp, err := keypair.ParseSeed(p.Signer)
		if err != nil {
			return nil, errors.Wrap(err, "failed to parse seed")
		}
		p.Address = kp.Address()
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		AssetRemover Pipeline
	}{
		ctx.Common(),
		ns,
		p,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	return []io.Reader{&b}, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if err := ctx.Get("assetremover", &p); err != nil {
		return nil, err
	}

	if p.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate signer")
		}
		p.Signer = kp.Seed()
		p.Address = kp.Address()
	}

	if err := p.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to populate Sentry")
	}

	return p, nil
}
