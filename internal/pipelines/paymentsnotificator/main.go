package paymentsnotificator

import (
	"bytes"
	"io"
	"time"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/proxypayments"

	"gitlab.com/tokend/keypair"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	Image    string
	Database struct {
		URL   string `fig:"url" yaml:",omitempty"`
		Local bool   `fig:"local" yaml:",omitempty"`
	} `fig:"database" yaml:",omitempty"`
	Notificator struct {
		Endpoint     string `fig:"endpoint" yaml:",omitempty"`
		ClientRouter string `fig:"clientrouter" yaml:",omitempty"`
	} `fig:"notificator" yaml:",omitempty"`
	Cursor                   int                    `yaml:",omitempty"`
	RedemptionExpirationTime time.Duration          `fig:"redemptionexpirationtime" yaml:",omitempty"`
	Signer                   string                 `fig:"signer"`
	Sentry                   templates.SentryConfig `fig:"sentry"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "paymentsnotificator"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	box := packr.New("paymentsnotificator", ".")
	template := templates.MustGet(box, "paymentsnotificator.yaml")
	pgtemplate := templates.MustGet(box, "paymentsnotificator-pg.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	env := Env{
		RedemptionExpirationTime: 15552000 * time.Second,
	}
	if err := ctx.Get("paymentsnotificator", &env); err != nil {
		return nil, err
	}

	var result []io.Reader
	if env.Database.URL == "" && env.Database.Local {
		env.Database.URL = "postgres://paymentsnotificator:paymentsnotificator@paymentsnotificator-postgres/paymentsnotificator?sslmode=disable"
		var a bytes.Buffer
		da := struct {
			namespace.Env
			PaymentsNotificator Env
		}{
			ns,
			env,
		}
		err := pgtemplate.Execute(&a, da)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render pg template")
		}

		result = append(result, &a)
	}

	var proxyPayments proxypayments.Env
	if err := ctx.Get("proxypayments", &proxyPayments); err != nil {
		return nil, errors.Wrap(err, "failed to get proxypayments env")
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		PaymentsNotificator Env
		ProxyPayments       proxypayments.Env
	}{
		ctx.Common(),
		ns,
		env,
		proxyPayments,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)

	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var env Env
	if err := ctx.Get("paymentsnotificator", &env); err != nil {
		return nil, err
	}

	if env.Database.URL == "" {
		env.Database.Local = true
	}

	if env.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate signer")
		}
		env.Signer = kp.Seed()
	}

	if err := env.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to get Sentry DSN")
	}

	return env, nil
}
