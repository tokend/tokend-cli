package forbill

import (
	"bytes"
	"io"

	"gitlab.com/tokend/keypair"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/ingress"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	Image   string
	Signer  string `fig:"signer"`
	Forbill struct {
		APIKey           string `fig:"apikey"`
		Point            string `fig:"point"`
		Debug            bool   `yaml:",omitempty"`
		PaymentServiceID string `fig:"paymentserviceid"`
		AccountID        string `fig:"accountid"`
		WalletID         string `fig:"walletid"`
		PaymentsAsset    string `fig:"paymentsasset"`
		ClientURL        string `fig:"clienturl"`
	}
	Sentry templates.SentryConfig `fig:"sentry"`
	Cop    templates.CopConfig    `fig:"cop" yaml:",omitempty"`
	Janus  templates.JanusConfig  `fig:"janus" yaml:",omitempty"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "forbill"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	box := packr.New("forbill", ".")
	template := templates.MustGet(box, "forbill.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var ingressEnv ingress.Env
	if err := ctx.Get("ingress", &ingressEnv); err != nil {
		return nil, errors.Wrap(err, "failed to get ingress config")
	}

	var env Env
	if err := ctx.Get("forbill", &env); err != nil {
		return nil, err
	}

	if ctx.Common().MustTokenDRelease() >= types.TraefikCopTokenDRelease {
		if err := env.Cop.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate cop config")
		}
	} else {
		if err := env.Janus.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate janus config")
		}
	}

	var result []io.Reader
	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		Forbill Env
		Ingress ingress.Env
	}{
		ctx.Common(),
		ns,
		env,
		ingressEnv,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)

	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var env Env
	if err := ctx.Get("forbill", &env); err != nil {
		return nil, err
	}

	if env.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate signer")
		}
		env.Signer = kp.Seed()
	}

	if err := env.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to get Sentry DSN")
	}

	return env, nil
}
