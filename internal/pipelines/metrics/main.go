package metrics

import (
	"bytes"
	b64 "encoding/base64"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/ingress"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/webclients/envirements"
	"io"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	KubeDashboardConf struct {
		KubeDashboard bool   `fig:"enable" yaml:",omitempty"`
		URL           string `fig:"url" yaml:",omitempty"`
		Namespace     string `fig:"namespace" yaml:",omitempty"`
	} `fig:"kubedashboard" yaml:",omitempty"`
	GrafanaConf struct {
		Grafana   bool   `fig:"enable" yaml:",omitempty"`
		Username  string `fig:"username" yaml:",omitempty"`
		Password  string `fig:"password" yaml:",omitempty"`
		URL       string `fig:"url" yaml:",omitempty"`
		Namespace string `fig:"namespace" yaml:",omitempty"`
	} `fig:"grafana" yaml:",omitempty"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "metrics"
}
func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	box := packr.New("metrics", ".")
	template := templates.MustGet(box, "prometheus.yaml")
	grafana := templates.MustGet(box, "grafana.yaml")

	var env Env
	if err := ctx.Get("metrics", &env); err != nil {
		return nil, err
	}
	var nsEnv namespace.Env
	if err := ctx.Get("namespace", &nsEnv); err != nil {
		return nil, errors.Wrap(err, "failed to get namespace config")
	}
	if env.KubeDashboardConf.KubeDashboard == true {
		template = templates.MustGet(box, "kubedashboard.yaml")
		env.KubeDashboardConf.Namespace = nsEnv.Namespace
	}
	env.GrafanaConf.Username = b64.StdEncoding.EncodeToString([]byte(env.GrafanaConf.Username))
	env.GrafanaConf.Password = b64.StdEncoding.EncodeToString([]byte(env.GrafanaConf.Password))
	env.GrafanaConf.Namespace = nsEnv.Namespace

	var ingressEnv ingress.Env
	if err := ctx.Get("ingress", &ingressEnv, envirements.NodeHook); err != nil {
		return nil, errors.Wrap(err, "failed to get ingress config")
	}

	if err := ctx.Get("metrics", &p); err != nil {
		return nil, err
	}
	var result []io.Reader
	if env.GrafanaConf.Grafana == true {
		var a bytes.Buffer
		da := struct {
			Ingress ingress.Env
			Metrics Env
		}{
			ingressEnv,
			env,
		}
		err := grafana.Execute(&a, da)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render grafana template")
		}

		result = append(result, &a)
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		Ingress ingress.Env
		Metrics Env
	}{
		ctx.Common(),
		ingressEnv,
		env,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)

	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if err := ctx.Get("metrics", &p); err != nil {
		return nil, err
	}

	return p, nil
}
