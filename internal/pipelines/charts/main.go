package charts

import (
	"bytes"
	"io"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Pipeline struct {
	Image  string
	Signer string
	Sentry templates.SentryConfig `fig:"sentry"`
	Cop    templates.CopConfig    `fig:"cop" yaml:",omitempty"`
	Janus  templates.JanusConfig  `fig:"janus" yaml:",omitempty"`
}

func (p Pipeline) Name() string {
	return "charts"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("charts", "."), "charts.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	if err := ctx.Get("charts", &p); err != nil {
		return nil, err
	}

	if ctx.Common().MustTokenDRelease() >= types.TraefikCopTokenDRelease {
		if err := p.Cop.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate cop config")
		}
		p.Cop.ServicePrefix = "/charts"
	} else {
		if err := p.Janus.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate janus config")
		}
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		Charts Pipeline
	}{
		ctx.Common(),
		ns,
		p,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	return []io.Reader{&b}, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if err := ctx.Get("charts", &p); err != nil {
		return nil, err
	}

	if p.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate signer")
		}
		p.Signer = kp.Seed()
	}

	if err := p.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to populate Sentry")
	}

	return p, nil
}
