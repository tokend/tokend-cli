package envirements

import (
	"reflect"

	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/figure"
)

type Env struct {
	Nodes []NodeEnv `fig:"nodes"`
}

var NodeHook = figure.Hooks{
	"[]envirements.NodeEnv": func(value interface{}) (reflect.Value, error) {
		slice, err := cast.ToSliceE(value)
		if err != nil {
			return reflect.Value{}, err
		}
		r := []NodeEnv{}
		for _, n := range slice {
			a, err := cast.ToStringMapE(n)
			if err != nil {
				return reflect.Value{}, err
			}
			var node NodeEnv
			if err := figure.Out(&node).From(a).Please(); err != nil {
				return reflect.Value{}, err
			}
			r = append(r, node)
		}
		return reflect.ValueOf(r), nil
	},
}

type NodeEnv struct {
	Disposable bool   `fig:"disposable" yaml:",omitempty"`
	Image      string `yaml:"image,omitempty"`
	Domain     string `yaml:domain,omitempty`
	Name       string
	Config     struct {
		//HORIZON_SERVER:                            'https://{{ required $node.APIDomain }}',
		//KEY_SERVER_ADMIN:                          'https://{{ required $node.ADKSDomain }}',
		IOSManifestLink string `fig:"iosmanifestlink" yaml:",omitempty"`
		PlayMarketLink  string `fig:"playmarketlink" yaml:",omitempty"`
		ImagesBucketURL string `fig:"imagesbucketurl" yaml:",omitempty"`
		AppName         string `fig:"appname" yaml:",omitempty"`
		SupportEmail    string `fig:"supportemail" yaml:",omitempty"`
		SupportPhone    string `fig:"supportphone" yaml:",omitempty"`
		SupportUrl      string `fig:"supporturl" yaml:",omitempty"`
	}
}
