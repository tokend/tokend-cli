package webclients

import (
	"bytes"
	"fmt"
	"io"

	"github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/api"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/core"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/ingress"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/webclients/envirements"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "webclients"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("webclients", "."), "webclients.yaml")
	var result []io.Reader

	var nsEnv namespace.Env
	if err := ctx.Get("namespace", &nsEnv); err != nil {
		return nil, errors.Wrap(err, "failed to get namespace config")
	}

	var ingressEnv ingress.Env
	if err := ctx.Get("ingress", &ingressEnv, envirements.NodeHook); err != nil {
		return nil, errors.Wrap(err, "failed to get ingress config")
	}

	var apiEnv api.Env
	if err := ctx.Get("api", &apiEnv); err != nil {
		return nil, err
	}

	var coreEnv core.Env
	if err := ctx.Get("core", &coreEnv, core.NodeHook); err != nil {
		return nil, err
	}

	var env envirements.Env
	if err := ctx.Get("webclients", &env, envirements.NodeHook); err != nil {
		return nil, errors.Wrap(err, "failed to get webclient config")
	}

	var scheme string
	switch uc := ctx.Common().UseCase; uc {
	case "", "vanilla":
		scheme = "vanilla"
	case "health-care", "healthcare":
		scheme = "healthcare"
	case "reit", "real-estate":
		scheme = "reit"
	case "loyaltyPointsMerchant":
		scheme = "loyaltyPointsMerchant"
	case "loyaltyPointsReconciliation":
		scheme = "loyaltyPointsReconciliation"
	case "conto":
		scheme = "conto"
	default:
		return nil, fmt.Errorf("unknown use case: %v", uc)
	}

	var webclients envirements.Env
	if err := ctx.Get("namespace", &webclients, envirements.NodeHook); err != nil {
		return nil, errors.Wrap(err, "failed to get namespace config")
	}

	for _, node := range env.Nodes {

		var b bytes.Buffer
		d := struct {
			types.Common
			namespace.Env
			API          api.Env
			Core         core.Env
			Ingress      ingress.Env
			Node         envirements.NodeEnv
			WebClients   envirements.Env
			ModuleScheme string
		}{
			ctx.Common(),
			nsEnv,
			apiEnv,
			coreEnv,
			ingressEnv,
			node,
			env,
			scheme,
		}

		err := template.Execute(&b, d)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render template")
		}
		result = append(result, &b)
	}
	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	env := envirements.Env{}
	if err := ctx.Get("webclients", &env, envirements.NodeHook); err != nil {
		return nil, errors.Wrap(err, "failed to get web config")
	}

	for idx := range env.Nodes {
		node := &env.Nodes[idx]
		if node.Name == "" {
			node.Name = fmt.Sprintf("web-%s%s", node.Domain, ctx.RandomString())
		}
	}

	return env, nil
}
