package locator

import (
	"bytes"
	"io"

	"github.com/gobuffalo/packr/v2"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/ingress"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	Image  string
	Sentry templates.SentryConfig `fig:"sentry"`
	Cop    templates.CopConfig    `fig:"cop" yaml:",omitempty"`
	Janus  templates.JanusConfig  `fig:"janus" yaml:",omitempty"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "locator"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	box := packr.New("locator", ".")
	template := templates.MustGet(box, "locator.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var ingressEnv ingress.Env
	if err := ctx.Get("ingress", &ingressEnv); err != nil {
		return nil, errors.Wrap(err, "failed to get ingress config")
	}

	var env Env
	if err := ctx.Get("locator", &env); err != nil {
		return nil, err
	}

	if ctx.Common().MustTokenDRelease() >= types.TraefikCopTokenDRelease {
		if err := env.Cop.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate cop config")
		}
	} else {
		if err := env.Janus.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate janus config")
		}
	}

	var result []io.Reader
	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		Locator Env
		Ingress ingress.Env
	}{
		ctx.Common(),
		ns,
		env,
		ingressEnv,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)

	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var env Env
	if err := ctx.Get("locator", &env); err != nil {
		return nil, err
	}

	if err := env.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to get Sentry DSN")
	}

	return env, nil
}
