package errhandler

import (
	"bytes"
	"io"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"

	"github.com/gobuffalo/packr/v2"

	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	Image  string
	Cop    templates.CopConfig    `fig:"cop" yaml:",omitempty"`
	Sentry templates.SentryConfig `fig:"sentry"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "errhandler"
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if ctx.Common().MustTokenDRelease() < types.TraefikCopTokenDRelease {
		return nil, nil
	}

	var env Env
	if err := ctx.Get("errhandler", &env); err != nil {
		return nil, err
	}

	if err := env.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to get Sentry DSN")
	}

	return env, nil
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	if ctx.Common().MustTokenDRelease() < types.TraefikCopTokenDRelease {
		return nil, nil
	}

	box := packr.New("errhandler", ".")
	template := templates.MustGet(box, "errhandler.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var env Env
	if err := ctx.Get("errhandler", &env); err != nil {
		return nil, err
	}

	if err := env.Cop.Populate(p.Name()); err != nil {
		return nil, errors.Wrap(err, "failed to populate cop config")
	}

	var b bytes.Buffer
	err := template.Execute(&b, struct {
		TokenDRelease string
		namespace.Env
		Errors Env
	}{
		ctx.Common().TokenDRelease,
		ns,
		env,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	return []io.Reader{&b}, nil
}
