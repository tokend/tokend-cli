{{ if not .Coinpayments.Deposit.Disabled }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: coinpaymentsdeposit
  namespace: {{ required .Namespace }}
  labels:
    app: coinpaymentsdeposit
    service: coinpaymentsdeposit
data:
  config.yaml:  |
                horizon:
{{ if atoi_ge .TokenDRelease "5" }}
                  endpoint:                              http://traefik
{{ else }}
                  endpoint:                              http://janus
{{ end }}                   
                  signer: {{ required .Coinpayments.Deposit.Signer }}

                {{ sentrysnippet .Coinpayments.Deposit.Sentry | indent 16 }}

                coinpayments:
                  address: https://www.coinpayments.net/api.php
                  # TODO: Leave it constant?
                  request_timeout: 30
                  merchant_id: {{ .Coinpayments.Credentials.MerchantID }}
                  private_key: {{ .Coinpayments.Credentials.PrivateKey }}
                  public_key: {{ .Coinpayments.Credentials.PublicKey }}
                  ipn_secret: {{ .Coinpayments.Credentials.IPNSecret }}

                deposit:
                  source: {{ required .Coinpayments.Deposit.Source }}
                  signer: {{ required .Coinpayments.Deposit.Signer }}

                listener:
                  addr: :80

                {{ apigatewaysnippet .TokenDRelease .Coinpayments.Deposit.Cop .Coinpayments.Deposit.Janus | indent 16 }}
---
apiVersion:                                  apps/v1
kind:                                        Deployment
metadata:
  name:                                      coinpaymentsdeposit
  namespace:                                 {{ required .Namespace }}
  labels:
    app:                                     coinpaymentsdeposit
spec:
  selector:
    matchLabels:
      app:                                   coinpaymentsdeposit
  template:
    metadata:
      labels:
        project:                             {{ required .Namespace }}
        app:                                 coinpaymentsdeposit
        service:                             coinpaymentsdeposit
    spec:
      restartPolicy:                         Always
      imagePullSecrets:
        - name:                              registry.gitlab.com
      volumes:
        - name:                              coinpaymentsdeposit
          configMap:
            name:                            coinpaymentsdeposit
      containers:
        - image:                             {{ with .Coinpayments.Deposit.Image }}.{{ else }}{{ required .Coinpayments.DefaultImage }}{{ end }}
          name:                              coinpaymentsdeposit
          env:
            - name: KV_VIPER_FILE
              value: /config/config.yaml
          args:                              ["deposit"]
          volumeMounts:
            - name:                          coinpaymentsdeposit
              mountPath:                     "/config"
---
kind:                                        Service
apiVersion:                                  v1
metadata:
  name:                                      coinpayments-deposit
  namespace:                                 {{ required .Namespace }}
spec:
  selector:
    app:                                     coinpaymentsdeposit
  ports:
    - protocol:                              TCP
      name:                                  coinpayments-deposit
      port:                                  80
      targetPort:                            80
{{ end }}
{{ if not .Coinpayments.DepositVerify.Disabled }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: coinpaymentsdepositverify
  namespace: {{ required .Namespace }}
  labels:
    app: coinpaymentsdepositverify
    service: coinpaymentsdepositverify
data:
  config.yaml:  |
                coinpayments:
                  address: https://www.coinpayments.net/api.php
                  # TODO: Leave it constant?
                  request_timeout: 30
                  merchant_id: {{ .Coinpayments.Credentials.MerchantID }}
                  private_key: {{ .Coinpayments.Credentials.PrivateKey }}
                  public_key: {{ .Coinpayments.Credentials.PublicKey }}
                  ipn_secret: {{ .Coinpayments.Credentials.IPNSecret }}
                deposit_verify:
                  source: {{ required .Coinpayments.DepositVerify.Source }}
                  signer: {{ required .Coinpayments.DepositVerify.Signer }}

                {{ sentrysnippet .Coinpayments.DepositVerify.Sentry | indent 16 }}

                horizon:
{{ if atoi_ge .TokenDRelease "5" }}
                  endpoint: http://traefik
{{ else }}
                  endpoint: http://janus
{{ end }}
                  signer: {{ required .Coinpayments.DepositVerify.Signer }}
---
apiVersion:                                  apps/v1
kind:                                        Deployment
metadata:
  name:                                      coinpaymentsdepositverify
  namespace:                                 {{ required .Namespace }}
  labels:
    app:                                     coinpaymentsdepositverify
spec:
  selector:
    matchLabels:
      app:                                   coinpaymentsdepositverify
  template:
    metadata:
      labels:
        project:                             {{ required .Namespace }}
        app:                                 coinpaymentsdepositverify
        service:                             coinpaymentsdepositverify
    spec:
      restartPolicy:                         Always
      imagePullSecrets:
        - name:                              registry.gitlab.com
      volumes:
        - name:                              coinpaymentsdepositverify
          configMap:
            name:                            coinpaymentsdepositverify
      containers:
        - image:                             {{ with .Coinpayments.DepositVerify.Image }} . {{ else }} {{ required .Coinpayments.DefaultImage }} {{ end }}
          name:                              coinpaymentsdepositverify
          env:
            - name: KV_VIPER_FILE
              value: /config/config.yaml
          args:                              ["deposit-verify"]
          volumeMounts:
            - name:                          coinpaymentsdepositverify
              mountPath:                     "/config"
{{ end }}
{{ if not .Coinpayments.Withdraw.Disabled }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: coinpaymentswithdraw
  namespace: {{ required .Namespace }}
  labels:
    app: coinpaymentswithdraw
    service: coinpaymentswithdraw
data:
  config.yaml:  |
                horizon:
{{ if atoi_ge .TokenDRelease "5" }}
                  endpoint: http://traefik
{{ else }}
                  endpoint: http://janus
{{ end }}
                  signer: {{ required .Coinpayments.Withdraw.Signer }}

                {{ sentrysnippet .Coinpayments.Withdraw.Sentry | indent 16 }}

                coinpayments:
                  address: https://www.coinpayments.net/api.php
                  # TODO: Leave it constant?
                  request_timeout: 30
                  merchant_id: {{ .Coinpayments.Credentials.MerchantID }}
                  private_key: {{ .Coinpayments.Credentials.PrivateKey }}
                  public_key: {{ .Coinpayments.Credentials.PublicKey }}
                  ipn_secret: {{ .Coinpayments.Credentials.IPNSecret }}

                withdraw:
                  source: {{ required .Coinpayments.Withdraw.Source }}
                  signer: {{ required .Coinpayments.Withdraw.Signer }}
                  add_tx_fee: {{ required .Coinpayments.Withdraw.AddTxFee }}
                  auto_confirm: {{ required .Coinpayments.Withdraw.AutoConfirm }}
---
apiVersion:                                  apps/v1
kind:                                        Deployment
metadata:
  name:                                      coinpaymentswithdraw
  namespace:                                 {{ required .Namespace }}
  labels:
    app:                                     coinpaymentswithdraw
spec:
  selector:
    matchLabels:
      app:                                   coinpaymentswithdraw
  template:
    metadata:
      labels:
        project:                             {{ required .Namespace }}
        app:                                 coinpaymentswithdraw
        service:                             coinpaymentswithdraw
    spec:
      restartPolicy:                         Always
      imagePullSecrets:
        - name:                              registry.gitlab.com
      volumes:
        - name:                              coinpaymentswithdraw
          configMap:
            name:                            coinpaymentswithdraw
      containers:
        - image:                             {{ with .Coinpayments.Withdraw.Image }} . {{ else }} {{ required .Coinpayments.DefaultImage }} {{ end }}
          name:                              coinpaymentswithdraw
          env:
            - name: KV_VIPER_FILE
              value: /config/config.yaml
          args:                              ["withdraw"]
          volumeMounts:
            - name:                          coinpaymentswithdraw
              mountPath:                     "/config"
{{ end }}
{{ if not .Coinpayments.AtomicSwapChecker.Disabled }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: coinpaymentsatomicswapchecker
  namespace: {{ required .Namespace }}
  labels:
    app: coinpaymentsatomicswapchecker
    service: coinpaymentsatomicswapchecker
data:
  config.yaml:  |
                horizon:
{{ if atoi_ge .TokenDRelease "5" }}
                  endpoint: http://traefik
{{ else }}
                  endpoint: http://janus
{{ end }}
                  signer: {{ required .Coinpayments.AtomicSwapChecker.Signer }}

                {{ sentrysnippet .Coinpayments.AtomicSwapChecker.Sentry | indent 16 }}

                coinpayments:
                  address: https://www.coinpayments.net/api.php
                  # TODO: Leave it constant?
                  request_timeout: 30
                  merchant_id: {{ .Coinpayments.Credentials.MerchantID }}
                  private_key: {{ .Coinpayments.Credentials.PrivateKey }}
                  public_key: {{ .Coinpayments.Credentials.PublicKey }}
                  ipn_secret: {{ .Coinpayments.Credentials.IPNSecret }}

                atomic_swap_checker:
                  signer: {{ required .Coinpayments.AtomicSwapChecker.Signer }}
---
apiVersion:                                  apps/v1
kind:                                        Deployment
metadata:
  name:                                      coinpaymentsatomicswapchecker
  namespace:                                 {{ required .Namespace }}
  labels:
    app:                                     coinpaymentsatomicswapchecker
spec:
  selector:
    matchLabels:
      app:                                   coinpaymentsatomicswapchecker
  template:
    metadata:
      labels:
        project:                             {{ required .Namespace }}
        app:                                 coinpaymentsatomicswapchecker
        service:                             coinpaymentsatomicswapchecker
    spec:
      restartPolicy:                         Always
      imagePullSecrets:
        - name:                              registry.gitlab.com
      volumes:
        - name:                              coinpaymentsatomicswapchecker
          configMap:
            name:                            coinpaymentsatomicswapchecker
      containers:
        - image:                             {{ with .Coinpayments.AtomicSwapChecker.Image }} . {{ else }} {{ required .Coinpayments.DefaultImage }} {{ end }}
          name:                              coinpaymentsatomicswapchecker
          env:
            - name: KV_VIPER_FILE
              value: /config/config.yaml
          args:                              ["atomic-swap-checker"]
          volumeMounts:
            - name:                          coinpaymentsatomicswapchecker
              mountPath:                     "/config"
{{ end }}
{{ if not .Coinpayments.AtomicSwapMatcher.Disabled }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: coinpaymentsatomicswapmatcher
  namespace: {{ required .Namespace }}
  labels:
    app: coinpaymentsatomicswapmatcher
    service: coinpaymentsatomicswapmatcher
data:
  config.yaml:  |
                horizon:
{{ if atoi_ge .TokenDRelease "5" }}
                  endpoint: http://traefik
{{ else }}
                  endpoint: http://janus
{{ end }}
                  signer: {{ required .Coinpayments.AtomicSwapMatcher.Signer }}

                {{ sentrysnippet .Coinpayments.AtomicSwapMatcher.Sentry | indent 16 }}

                coinpayments:
                  address: https://www.coinpayments.net/api.php
                  # TODO: Leave it constant?
                  request_timeout: 30
                  merchant_id: {{ .Coinpayments.Credentials.MerchantID }}
                  private_key: {{ .Coinpayments.Credentials.PrivateKey }}
                  public_key: {{ .Coinpayments.Credentials.PublicKey }}
                  ipn_secret: {{ .Coinpayments.Credentials.IPNSecret }}

                atomic_swap_matcher:
                  excluded_assets:
                    - UAH
                  signer: {{ required .Coinpayments.AtomicSwapMatcher.Signer }}
---
apiVersion:                                  apps/v1
kind:                                        Deployment
metadata:
  name:                                      coinpaymentsatomicswapmatcher
  namespace:                                 {{ required .Namespace }}
  labels:
    app:                                     coinpaymentsatomicswapmatcher
spec:
  selector:
    matchLabels:
      app:                                   coinpaymentsatomicswapmatcher
  template:
    metadata:
      labels:
        project:                             {{ required .Namespace }}
        app:                                 coinpaymentsatomicswapmatcher
        service:                             coinpaymentsatomicswapmatcher
    spec:
      restartPolicy:                         Always
      imagePullSecrets:
        - name:                              registry.gitlab.com
      volumes:
        - name:                              coinpaymentsatomicswapmatcher
          configMap:
            name:                            coinpaymentsatomicswapmatcher
      containers:
        - image:                             {{ with .Coinpayments.AtomicSwapMatcher.Image }} . {{ else }} {{ required .Coinpayments.DefaultImage }} {{ end }}
          name:                              coinpaymentsatomicswapmatcher
          env:
            - name: KV_VIPER_FILE
              value: /config/config.yaml
          args:                              ["atomic-swap-matcher"]
          volumeMounts:
            - name:                          coinpaymentsatomicswapmatcher
              mountPath:                     "/config"
{{ end }}
{{ if not .Coinpayments.AtomicSwap.Disabled }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: coinpaymentsatomicswap
  namespace: {{ required .Namespace }}
  labels:
    app: coinpaymentsatomicswap
    service: coinpaymentsatomicswap
data:
  config.yaml:  |
                horizon:
{{ if atoi_ge .TokenDRelease "5" }}
                  endpoint: http://traefik
{{ else }}
                  endpoint: http://janus
{{ end }}
                  signer: {{ required .Coinpayments.AtomicSwap.Signer }}

                {{ sentrysnippet .Coinpayments.AtomicSwap.Sentry | indent 16 }}

                listener:
                  addr: :80

                {{ apigatewaysnippet .TokenDRelease .Coinpayments.AtomicSwap.Cop .Coinpayments.AtomicSwap.Janus | indent 16 }}

                coinpayments:
                  merchant_id: {{ .Coinpayments.Credentials.MerchantID }}
                  address: https://www.coinpayments.net/api.php
                  private_key: {{ .Coinpayments.Credentials.PrivateKey }}
                  public_key: {{ .Coinpayments.Credentials.PublicKey }}
                  ipn_secret: {{ .Coinpayments.Credentials.IPNSecret }}
                  request_timeout: 30

                atomic_swap:
{{ if atoi_ge .TokenDRelease "5" }}
                  forbill_endpoint: http://traefik
{{ else }}
                  forbill_endpoint: http://janus
{{ end }}
                  signer: {{ required .Coinpayments.AtomicSwap.Signer }}
                  price_asset: UAH
---
apiVersion:                                  apps/v1
kind:                                        Deployment
metadata:
  name:                                      coinpaymentsatomicswap
  namespace:                                 {{ required .Namespace }}
  labels:
    app:                                     coinpaymentsatomicswap
spec:
  selector:
    matchLabels:
      app:                                   coinpaymentsatomicswap
  template:
    metadata:
      labels:
        project:                             {{ required .Namespace }}
        app:                                 coinpaymentsatomicswap
        service:                             coinpaymentsatomicswap
    spec:
      restartPolicy:                         Always
      imagePullSecrets:
        - name:                              registry.gitlab.com
      volumes:
        - name:                              coinpaymentsatomicswap
          configMap:
            name:                            coinpaymentsatomicswap
      containers:
        - image:                             {{ with .Coinpayments.AtomicSwap.Image }} . {{ else }} {{ required .Coinpayments.DefaultImage }} {{ end }}
          name:                              coinpaymentsatomicswap
          env:
            - name: KV_VIPER_FILE
              value: /config/config.yaml
          args:                              ["atomic-swap"]
          volumeMounts:
            - name:                          coinpaymentsatomicswap
              mountPath:                     "/config"
---
kind:                                        Service
apiVersion:                                  v1
metadata:
  name:                                      coinpaymentsatomicswap
  namespace:                                 {{ required .Namespace }}
spec:
  selector:
    app:                                     coinpaymentsatomicswap
  ports:
    - protocol:                              TCP
      name:                                  coinpaymentsatomicswap
      port:                                  80
      targetPort:                            80

{{ end }}
