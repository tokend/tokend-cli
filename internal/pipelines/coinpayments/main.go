package coinpayments

import (
	"bytes"
	"fmt"
	"io"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"

	"github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/keypair"

	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

// TODO: Split it
type Pipeline struct {
	DefaultImage string `fig:"defaultimage" yaml:",omitempty"`
	Credentials  struct {
		Image      string `yaml:",omitempty"`
		MerchantID string `fig:"merchantid"`
		PrivateKey string `fig:"privatekey"`
		PublicKey  string `fig:"publickey"`
		IPNSecret  string `fig:"ipnsecret"`
	}
	Deposit struct {
		Image    string `yaml:",omitempty"`
		Signer   string `yaml:",omitempty"`
		Source   string
		Sentry   templates.SentryConfig `fig:"sentry"`
		Cop      templates.CopConfig    `fig:"cop" yaml:",omitempty"`
		Janus    templates.JanusConfig  `fig:"janus" yaml:",omitempty"`
		Disabled bool                   `yaml:",omitempty"`
	}
	DepositVerify struct {
		Image    string `yaml:",omitempty"`
		Signer   string `yaml:",omitempty"`
		Source   string
		Sentry   templates.SentryConfig `fig:"sentry"`
		Disabled bool                   `yaml:",omitempty"`
	} `fig:"depositverify"`
	Withdraw struct {
		Image       string `yaml:",omitempty"`
		Signer      string `yaml:",omitempty"`
		Source      string
		AddTxFee    bool                   `fig:"addtxfee"`
		AutoConfirm bool                   `fig:"autoconfirm"`
		Sentry      templates.SentryConfig `fig:"sentry"`
		Disabled    bool                   `yaml:",omitempty"`
	}
	AtomicSwapChecker struct {
		Image    string                 `yaml:",omitempty"`
		Signer   string                 `yaml:",omitempty"`
		Sentry   templates.SentryConfig `fig:"sentry"`
		Disabled bool                   `yaml:",omitempty"`
	} `fig:"atomicswapchecker"`
	AtomicSwapMatcher struct {
		Image    string                 `yaml:",omitempty"`
		Signer   string                 `yaml:",omitempty"`
		Sentry   templates.SentryConfig `fig:"sentry"`
		Disabled bool                   `yaml:",omitempty"`
	} `fig:"atomicswapmatcher"`
	AtomicSwap struct {
		Image    string                 `yaml:",omitempty"`
		Signer   string                 `yaml:",omitempty"`
		Sentry   templates.SentryConfig `fig:"sentry"`
		Cop      templates.CopConfig    `fig:"cop" yaml:",omitempty"`
		Janus    templates.JanusConfig  `fig:"janus" yaml:",omitempty"`
		Disabled bool                   `yaml:",omitempty"`
	} `fig:"atomicswap"`
}

func (p Pipeline) Name() string {
	return "coinpayments"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("coinpayments", "."), "coinpayments.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	if err := ctx.Get("coinpayments", &p); err != nil {
		return nil, err
	}

	if ctx.Common().MustTokenDRelease() >= types.TraefikCopTokenDRelease {
		if err := p.Deposit.Cop.Populate(fmt.Sprintf("%s.deposit", p.Name())); err != nil {
			return nil, errors.Wrap(err, "failed to populate cop config")
		}
		p.Deposit.Cop.ServicePrefix = "/integrations/coinpayments/deposit"
		p.Deposit.Cop.ServiceName = "coinpayments-deposit"
		p.Deposit.Cop.Upstream = "http://coinpayments-deposit"
		if err := p.AtomicSwap.Cop.Populate(fmt.Sprintf("%s.atomicswap", p.Name())); err != nil {
			return nil, errors.Wrap(err, "failed to populate cop config")
		}
	} else {
		if err := p.Deposit.Janus.Populate(fmt.Sprintf("%s.deposit", p.Name())); err != nil {
			return nil, errors.Wrap(err, "failed to populate janus config")
		}
		if err := p.AtomicSwap.Janus.Populate(fmt.Sprintf("%s.atomicswap", p.Name())); err != nil {
			return nil, errors.Wrap(err, "failed to populate janus config")
		}
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		Coinpayments Pipeline
	}{
		ctx.Common(),
		ns,
		p,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	return []io.Reader{&b}, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if err := ctx.Get("coinpayments", &p, templates.CopPrefixHook); err != nil {
		return nil, err
	}

	if err := p.populateDeposit(ctx); err != nil {
		return nil, err
	}

	if err := p.populateDepositVerify(ctx); err != nil {
		return nil, err
	}

	if err := p.populateWithdraw(ctx); err != nil {
		return nil, err
	}

	if err := p.populateAtomicSwapChecker(ctx); err != nil {
		return nil, err
	}

	if err := p.populateAtomicSwapMatcher(ctx); err != nil {
		return nil, err
	}

	if err := p.populateAtomicSwap(ctx); err != nil {
		return nil, err
	}

	//// TODO: Why?
	//var coreEnv core.Env
	//if err := ctx.Get("core", &coreEnv, core.NodeHook); err != nil {
	//	return nil, err
	//}

	return p, nil
}

func (p Pipeline) populateDeposit(ctx types.Context) error {
	if p.Deposit.Disabled {
		return nil
	}
	if p.Deposit.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return errors.Wrap(err, "failed to generate signer")
		}
		p.Deposit.Signer = kp.Seed()
		if p.Deposit.Source != "" {
			return errors.New("Deposit.Signer and Deposit.Source are keypair. They both must be provided or neither of them must be provided")
		}
		p.Deposit.Source = kp.Address()
	}

	if err := p.Deposit.Sentry.Populate(p.Name()+".deposit", ctx); err != nil {
		return errors.Wrap(err, "failed to populate Sentry for coinpayments deposit")
	}

	return nil
}

func (p Pipeline) populateDepositVerify(ctx types.Context) error {
	if p.DepositVerify.Disabled {
		return nil
	}

	if p.DepositVerify.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return errors.Wrap(err, "failed to generate signer")
		}
		p.DepositVerify.Signer = kp.Seed()
		if p.DepositVerify.Source != "" {
			return errors.New("DepositVerify.Signer and DepositVerify.Source are keypair. They both must be provided or neither of them must be provided")
		}
		p.DepositVerify.Source = kp.Address()
	}

	if err := p.DepositVerify.Sentry.Populate(p.Name()+".depositverify", ctx); err != nil {
		return errors.Wrap(err, "failed to populate Sentry for coinpayments depositverify")
	}

	return nil
}

func (p Pipeline) populateWithdraw(ctx types.Context) error {
	if p.Withdraw.Disabled {
		return nil
	}
	if p.Withdraw.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return errors.Wrap(err, "failed to generate signer")
		}
		p.Withdraw.Signer = kp.Seed()
		if p.Withdraw.Source != "" {
			return errors.New("Withdraw.Signer and Withdraw.Source are keypair. They both must be provided or neither of them must be provided")
		}
		p.Withdraw.Source = kp.Address()
	}

	if err := p.Withdraw.Sentry.Populate(p.Name()+".withdraw", ctx); err != nil {
		return errors.Wrap(err, "failed to populate Sentry for coinpayments withdraw")
	}

	return nil
}

func (p Pipeline) populateAtomicSwapChecker(ctx types.Context) error {
	if p.AtomicSwapChecker.Disabled {
		return nil
	}

	if p.AtomicSwapChecker.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return errors.Wrap(err, "failed to generate signer")
		}
		p.AtomicSwapChecker.Signer = kp.Seed()
	}

	if err := p.AtomicSwapChecker.Sentry.Populate(p.Name()+".atomicswapchecker", ctx); err != nil {
		return errors.Wrap(err, "failed to populate Sentry for coinpayments atomicswapchecker")
	}

	return nil
}

func (p Pipeline) populateAtomicSwapMatcher(ctx types.Context) error {
	if p.AtomicSwapMatcher.Disabled {
		return nil
	}

	if p.AtomicSwapMatcher.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return errors.Wrap(err, "failed to generate signer")
		}
		p.AtomicSwapMatcher.Signer = kp.Seed()
	}

	if err := p.AtomicSwapMatcher.Sentry.Populate(p.Name()+".atomicswapmatcher", ctx); err != nil {
		return errors.Wrap(err, "failed to populate Sentry for coinpayments atomicswapmatcher")
	}

	return nil
}

func (p Pipeline) populateAtomicSwap(ctx types.Context) error {
	if p.AtomicSwap.Disabled {
		return nil
	}

	if p.AtomicSwap.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return errors.Wrap(err, "failed to generate signer")
		}
		p.AtomicSwap.Signer = kp.Seed()
	}

	if err := p.AtomicSwap.Sentry.Populate(p.Name()+".atomicswap", ctx); err != nil {
		return errors.Wrap(err, "failed to populate Sentry for coinpayments atomicswap")
	}

	return nil
}
