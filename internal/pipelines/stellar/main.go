package stellar

import (
	"bytes"
	"io"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Pipeline struct {
	Signer string
	Owner  string

	DepositImage  string `fig:"depositimage"`
	WithdrawImage string `fig:"withdrawimage"`

	Testnet          bool
	HotWalletAddress string `fig:"hotwalletaddress"`
	HotWalletSeed    string `fig:"hotwalletseed"`
	MaxBaseFee       uint32 `fig:"maxbasefee" yaml:",omitempty"`

	Sentry templates.SentryConfig `fig:"sentry"`
}

func (p Pipeline) Name() string {
	return "stellar"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("stellar", "."), "stellar.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	if err := ctx.Get("stellar", &p); err != nil {
		return nil, err
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		Stellar Pipeline
	}{
		ctx.Common(),
		ns,
		p,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	return []io.Reader{&b}, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if err := ctx.Get("stellar", &p); err != nil {
		return nil, err
	}

	if p.MaxBaseFee == 0 {
		p.MaxBaseFee = 200
	}

	if err := p.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to populate Sentry")
	}

	return p, nil
}
