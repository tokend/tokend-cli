package salecloser

import (
	"bytes"
	"io"

	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/core"
	"gitlab.com/tokend/tokend-cli/types"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
)

type Env struct {
	Image  string
	Signer string
	Sentry templates.SentryConfig `fig:"sentry"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "salecloser"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("salecloser", "."), "salecloser.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var coreEnv core.Env
	if err := ctx.Get("core", &coreEnv, core.NodeHook); err != nil {
		return nil, err
	}

	var env Env
	if err := ctx.Get("salecloser", &env); err != nil {
		return nil, err
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		SaleCloser Env
		Core       core.Env
	}{
		ctx.Common(),
		ns,
		env,
		coreEnv,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	return []io.Reader{&b}, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var env Env
	if err := ctx.Get("salecloser", &env); err != nil {
		return nil, err
	}

	if env.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate signer")
		}
		env.Signer = kp.Seed()
	}

	if err := env.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to get Sentry DSN")
	}

	return env, nil
}
