package scheduler

import (
	"bytes"
	"io"

	"github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	Image    string
	Database struct {
		URL   string `fig:"url" yaml:",omitempty"`
		Local bool   `fig:"local" yaml:",omitempty"`
	} `fig:"database" yaml:",omitempty"`
	Signer           string `fig:"signer"`
	Account          string
	GenerateTill     string                 `fig:"generatetill" yaml:",omitempty"`
	GenerationPeriod string                 `fig:"generationperiod" yaml:",omitempty"`
	Sentry           templates.SentryConfig `fig:"sentry"`
	Cop              templates.CopConfig    `fig:"cop" yaml:",omitempty"`
	Janus            templates.JanusConfig  `fig:"janus" yaml:",omitempty"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "scheduler"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	box := packr.New("scheduler", ".")
	template := templates.MustGet(box, "scheduler.yaml")
	pgtemplate := templates.MustGet(box, "scheduler-pg.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	env := Env{
		GenerateTill:     "12h",
		GenerationPeriod: "1m",
	}
	if err := ctx.Get("scheduler", &env); err != nil {
		return nil, err
	}

	var result []io.Reader
	if env.Database.URL == "" && env.Database.Local {
		env.Database.URL = "postgres://scheduler:scheduler@scheduler-postgres/scheduler?sslmode=disable"
		var a bytes.Buffer
		da := struct {
			namespace.Env
			Scheduler Env
		}{
			ns,
			env,
		}
		err := pgtemplate.Execute(&a, da)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render pg template")
		}

		result = append(result, &a)
	}

	if ctx.Common().MustTokenDRelease() >= types.TraefikCopTokenDRelease {
		if err := env.Cop.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate cop config")
		}
	} else {
		if err := env.Janus.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate janus config")
		}
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		Scheduler Env
	}{
		ctx.Common(),
		ns,
		env,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)

	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	var env Env
	if err := ctx.Get("scheduler", &env); err != nil {
		return nil, err
	}

	if env.Database.URL == "" {
		env.Database.Local = true
	}

	if env.Signer == "" {
		kp, err := keypair.Random()
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate signer")
		}
		env.Signer = kp.Seed()
		env.Account = kp.Address()
	}

	if env.Account == "" {
		kp, err := keypair.ParseSeed(env.Signer)
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate signer")
		}
		env.Account = kp.Address()
	}

	if err := env.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to get Sentry DSN")
	}

	return env, nil
}
