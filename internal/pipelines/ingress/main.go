package ingress

import (
	"bytes"
	"fmt"
	"io"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/webclients/envirements"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Env struct {
	CertificateARN      string   `fig:"certificatearn"`
	BaseDomain          string   `fig:"basedomain"`
	APIDomain           string   `fig:"apidomain"`
	ADKSDomain          string   `fig:"adksdomain"`
	HistoryDomain       string   `fig:"historydomain"`
	AuditHistoryDomain  string   `fig:"audithistorydomain"`
	MaxBodySize         string   `fig:"maxbodysize" yaml:",omitempty"`
	LocalhostEnabled    bool     `fig:"localhostenabled" yaml:",omitempty"`
	CORSAllowedDomains  []string `fig:"corsalloweddomains" yaml:",omitempty"`
	IPWhitelistRequired bool     `fig:"ipwhitelistrequired"`
	RestoreRealIPs      bool     `fig:"restorerealips"`
	WhitelistedIPs      []string `fig:"whitelistedips"`
	RealIPHeader        string   `fig:"realipheader"`
	DomainsList         []string `fig:"-"`
	CloudFlare          bool     `fig:"cloudflare"`
}

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "ingress"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("nginx", "."), "nginx-distributed.yaml")

	var nsEnv namespace.Env
	if err := ctx.Get("namespace", &nsEnv); err != nil {
		return nil, errors.Wrap(err, "failed to get namespace config")
	}

	var env Env
	if err := ctx.Get("ingress", &env); err != nil {
		return nil, err
	}

	var webcliEnv envirements.Env
	if err := ctx.Get("webclients", &webcliEnv, envirements.NodeHook); err != nil {
		return nil, err
	}
	env.DomainsList = make([]string, 0, len(webcliEnv.Nodes)*2)
	for idx := range webcliEnv.Nodes {
		node := &webcliEnv.Nodes[idx]
		env.DomainsList = append(env.DomainsList, "https://"+node.Domain+" 1;")
		env.DomainsList = append(env.DomainsList, "http://"+node.Domain+" 1;")
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		WebcliEnv envirements.Env
		Ingress   Env
	}{
		ctx.Common(),
		nsEnv,
		webcliEnv,
		env,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	return []io.Reader{&b}, nil
}

func DomainPrint(ctx types.Context, list []string) {
	var env Env
	env.DomainsList = list
	fmt.Println(list)

}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var env Env
	if err := ctx.Get("ingress", &env); err != nil {
		return nil, err
	}

	if env.APIDomain == "" {
		env.APIDomain = fmt.Sprintf("api.%s", env.BaseDomain)
	}

	if env.ADKSDomain == "" {
		env.ADKSDomain = fmt.Sprintf("adks.%s", env.BaseDomain)
	}

	if env.HistoryDomain == "" {
		env.HistoryDomain = fmt.Sprintf("history.%s", env.BaseDomain)
	}
	if env.RealIPHeader == "" {
		env.RealIPHeader = "X-Forwarder-For"
	}

	if env.AuditHistoryDomain == "" {
		env.AuditHistoryDomain = fmt.Sprintf("archive.%s", env.BaseDomain)
	}

	if env.MaxBodySize == "" {
		env.MaxBodySize = "32m"
	}

	return env, nil
}
