package erc20

import (
	"bytes"
	"io"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Pipeline struct {
	Ethereum struct {
		NodeRPC       string `fig:"noderpc"`
		NodeWS        string `fig:"nodews"`
		Checkpoint    int
		Confirmations int
		PrivateKey    string `fig:"privatekey"`
		HotWallet     string `fig:"hotwallet"`
	}

	Funnel struct {
		GasPrice  string `fig:"gasprice"`
		Threshold string
	}

	ContractDeployer struct {
		GasPrice      string `fig:"gasprice"`
		GasLimit      string `fig:"gaslimit"`
		ContractCount int    `fig:"contractcount"`
	} `fig:"contractdeployer"`

	Withdraw struct {
		GasPrice string `fig:"gasprice"`
		GasLimit string `fig:"gaslimit"`
	}

	Signer string
	Owner  string

	DepositImage  string `fig:"depositimage"`
	WithdrawImage string `fig:"withdrawimage"`

	Sentry templates.SentryConfig `fig:"sentry"`
}

func (p Pipeline) Name() string {
	return "erc20"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("erc20", "."), "erc20.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	if err := ctx.Get("erc20", &p); err != nil {
		return nil, err
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		ERC20 Pipeline
	}{
		ctx.Common(),
		ns,
		p,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	return []io.Reader{&b}, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if err := ctx.Get("erc20", &p); err != nil {
		return nil, err
	}

	if p.Withdraw.GasPrice == "" {
		p.Withdraw.GasPrice = "20"
	}

	if p.Withdraw.GasLimit == "" {
		p.Withdraw.GasLimit = "20000"
	}

	if p.Funnel.GasPrice == "" {
		p.Funnel.GasPrice = "20"
	}

	if p.Funnel.Threshold == "" {
		p.Funnel.GasPrice = "50000000"
	}

	if p.ContractDeployer.GasPrice == "" {
		p.ContractDeployer.GasPrice = "20"
	}

	if p.ContractDeployer.GasLimit == "" {
		p.ContractDeployer.GasLimit = "20000"
	}

	if err := p.Sentry.Populate(p.Name(), ctx); err != nil {
		return nil, errors.Wrap(err, "failed to populate Sentry")
	}

	return p, nil
}
