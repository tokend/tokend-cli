package pre3modules

import (
	"bytes"
	"io"
	"reflect"

	"github.com/gobuffalo/packr/v2"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Module struct {
	Config string
	Image  string
}

var Pre3ModuleHook = figure.Hooks{
	"map[string]pre3modules.Module": func(value interface{}) (reflect.Value, error) {
		m, err := cast.ToStringMapE(value)
		if err != nil {
			return reflect.Value{}, errors.Wrap(err, "failed to cast to map")
		}
		r := map[string]Module{}
		for k, v := range m {
			var module Module
			rawm, err := cast.ToStringMapE(v)
			if err != nil {
				return reflect.Value{}, errors.Wrap(err, "failed to cast module", logan.F{
					"module": k,
				})
			}
			if err := figure.Out(&module).From(rawm).Please(); err != nil {
				return reflect.Value{}, errors.Wrap(err, "failed to figure module", logan.F{
					"module": k,
				})
			}
			r[k] = module
		}
		return reflect.ValueOf(r), nil
	},
}

type Pipeline struct {
	Modules map[string]Module
}

func (p Pipeline) Name() string {
	return "pre3modules"
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if err := ctx.Get("pre3modules", &p, Pre3ModuleHook); err != nil {
		return nil, err
	}

	return p, nil
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("pre3modules", "."), "pre3modules.yaml")

	if err := ctx.Get("pre3modules", &p, Pre3ModuleHook); err != nil {
		return nil, err
	}

	var env namespace.Env
	if err := ctx.Get("namespace", &env); err != nil {
		return nil, err
	}

	var result []io.Reader
	for name, module := range p.Modules {
		var b bytes.Buffer
		d := struct {
			types.Common
			namespace.Env
			ModuleName string
			Module     Module
		}{
			ctx.Common(),
			env,
			name,
			module,
		}

		err := template.Execute(&b, d)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render template")
		}

		result = append(result, &b)
	}

	return result, nil
}
