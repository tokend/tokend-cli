package horizon

import (
	"bytes"
	"fmt"
	"io"
	"reflect"

	"github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/core"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Pipeline struct{}

func (p Pipeline) Name() string {
	return "horizon"
}

type Env struct {
	DefaultImage                string                 `fig:"defaultimage"`
	DefaultPersistent           bool                   `fig:"defaultpersistent" yaml:",omitempty"`
	DefaultPersistentVolumeSize string                 `fig:"defaultpersistentvolumesize" yaml:",omitempty"`
	DefaultSentry               templates.SentryConfig `fig:"defaultsentry" yaml:",omitempty"`
	CacheSize                   int64                  `fig:"cachesize" yaml:",omitempty"`
	CachePeriod                 string                 `fig:"cacheperiod" yaml:",omitempty"`
	Nodes                       []NodeEnv              `fig:"nodes"`
}

var NodeHook = figure.Hooks{
	"[]horizon.NodeEnv": func(value interface{}) (reflect.Value, error) {
		slice, err := cast.ToSliceE(value)
		if err != nil {
			return reflect.Value{}, err
		}
		var r []NodeEnv
		for _, n := range slice {
			a, err := cast.ToStringMapE(n)
			if err != nil {
				return reflect.Value{}, err
			}
			var node NodeEnv
			if err := figure.Out(&node).From(a).Please(); err != nil {
				return reflect.Value{}, err
			}
			r = append(r, node)
		}
		return reflect.ValueOf(r), nil
	},
}

type NodeEnv struct {
	Name  string
	Core  string
	Image string `yaml:"image,omitempty"`
	// Persistent horizon node will store history in persistent volume
	Persistent           *bool  `fig:"persistent" yaml:",omitempty"`
	PersistentVolumeSize string `fig:"persistentvolumesize"`
	// FIXME: at the time of writing figure shits itself on ptr types with nil values
	Sentry *templates.SentryConfig `fig:"-" yaml:"-"`
	Cop    templates.CopConfig     `fig:"cop" yaml:",omitempty"`
	Janus  templates.JanusConfig   `fig:"janus" yaml:",omitempty"`
	Limits templates.LimitsConfig  `fig:"limits" yaml:",omitempty"`
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("horizon", "."), "horizon.yaml")

	var result []io.Reader

	env := Env{
		CachePeriod: "1s",
		CacheSize:   1,
	}
	if err := ctx.Get("horizon", &env, NodeHook); err != nil {
		return nil, err
	}

	var nsEnv namespace.Env
	if err := ctx.Get("namespace", &nsEnv); err != nil {
		return nil, errors.Wrap(err, "failed to get namespace config")
	}

	for _, n := range env.Nodes {
		n := n

		if n.Sentry == nil {
			n.Sentry = &env.DefaultSentry
		}

		if n.PersistentVolumeSize == "" {
			n.PersistentVolumeSize = env.DefaultPersistentVolumeSize
		}

		if n.Limits.Resources.Limits.CPU == "" {
			n.Limits.Resources.Limits.CPU = "1"
		}

		if n.Limits.Resources.Limits.Memory == "" {
			n.Limits.Resources.Limits.Memory = "1000Mi"
		}

		if n.Limits.Resources.Requests.CPU == "" {
			n.Limits.Resources.Requests.CPU = "0.1"
		}

		if n.Limits.Resources.Requests.Memory == "" {
			n.Limits.Resources.Requests.Memory = "50Mi"
		}

		if ctx.Common().MustTokenDRelease() >= types.TraefikCopTokenDRelease {
			if err := n.Cop.Populate(n.Name); err != nil {
				return nil, errors.Wrap(err, "failed to populate cop config")
			}
		} else {
			if err := n.Janus.Populate(n.Name); err != nil {
				return nil, errors.Wrap(err, "failed to populate janus config")
			}
		}

		var b bytes.Buffer
		if n.Image == "" {
			n.Image = env.DefaultImage
		}
		if n.Persistent == nil {
			n.Persistent = &env.DefaultPersistent
		}
		err := template.Execute(&b, struct {
			types.Common
			namespace.Env
			Node    NodeEnv
			Horizon Env
		}{
			ctx.Common(),
			nsEnv,
			n,
			env,
		},
		)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render template")
		}
		result = append(result, &b)
	}

	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	var env Env
	if err := ctx.Get("horizon", &env, NodeHook); err != nil {
		return nil, errors.Wrap(err, "failed to get horizon config")
	}

	if len(env.Nodes) > 0 {
		return env, nil
	}

	var coreEnv core.Env
	if err := ctx.Get("core", &coreEnv, core.NodeHook); err != nil {
		return nil, errors.Wrap(err, "failed to get core config")
	}

	env.Nodes = make([]NodeEnv, len(coreEnv.Nodes))

	for idx := range coreEnv.Nodes {
		nodeName := fmt.Sprintf("horizon-%s", ctx.RandomString())
		env.Nodes[idx].Name = nodeName
		//env.Nodes[idx].Image = env.DefaultImage
		env.Nodes[idx].Core = coreEnv.Nodes[idx].Name
	}

	return env, nil
}
