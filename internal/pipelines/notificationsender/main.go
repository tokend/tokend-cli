package notificationsender

import (
	"bytes"
	"io"
	"reflect"

	packr "github.com/gobuffalo/packr/v2"
	"github.com/pkg/errors"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/templates"
	"gitlab.com/tokend/tokend-cli/types"
)

type Pipeline struct {
	Image          string
	Signer         string
	DefaultChannel string `fig:"defaultchannel"`
	Database       struct {
		URL             string `fig:"url" yaml:",omitempty"`
		MigrationsCount string `fig:"migrationscount"`
		Local           bool   `fig:"local" yaml:",omitempty"`
	} `fig:"database" yaml:",omitempty"`
	Mailgun struct {
		Key       string `fig:"key"`
		PublicKey string `fig:"publickey"`
		Domain    string `fig:"domain"`
		FromEmail string `fig:"fromemail"`
		FromName  string `fig:"fromname"`
	} `fig:"mailgun"`
	S3 struct {
		URL                  string
		Bucket               string
		AccessKey            string `fig:"accesskey"`
		SecretKey            string `fig:"secretkey"`
		Region               string
		DisableSSL           bool `fig:"disablessl" yaml:",omitempty"`
		ForcePathStyle       bool `fig:"s3forcepathstyle" yaml:",omitempty"`
		UseStaticCredentials bool `fig:"usestaticcredentials" yaml:",omitempty"`
	} `fig:"s3"`
	Firebase struct {
		ProjectID          string `fig:"projectid"`
		ServiceAccountID   string `fig:"serviceaccountid"`
		AccessFile         string `fig:"accessfile"`
		AndroidPackageName string `fig:"androidpackagename"`
	}
	NotificationTemplates []NotificationTemplate `fig:"templates"`
	Sentry                templates.SentryConfig `fig:"sentry"`
	Cop                   templates.CopConfig    `fig:"cop" yaml:",omitempty"`
	Janus                 templates.JanusConfig  `fig:"janus" yaml:",omitempty"`
}

type NotificationTemplate struct {
	Name    string
	Subject string `yaml:",omitempty"`
	Email   string `yaml:",omitempty"`
	Push    string `yaml:",omitempty"`
}

// NotificationHook to help figure to figure things out
var NotificationHook = figure.Hooks{
	"[]notificationsender.NotificationTemplate": func(value interface{}) (reflect.Value, error) {
		slice, err := cast.ToSliceE(value)
		if err != nil {
			return reflect.Value{}, err
		}
		r := []NotificationTemplate{}
		for _, n := range slice {
			a, err := cast.ToStringMapE(n)
			if err != nil {
				return reflect.Value{}, err
			}
			var notifTmpl NotificationTemplate
			if err := figure.Out(&notifTmpl).From(a).Please(); err != nil {
				return reflect.Value{}, err
			}
			r = append(r, notifTmpl)
		}
		return reflect.ValueOf(r), nil
	},
}

func (p Pipeline) Name() string {
	return "notificationsender"
}

func (p Pipeline) Deployment(ctx types.Context) ([]io.Reader, error) {
	template := templates.MustGet(packr.New("notificationsender", "."), "notificationsender.yaml")
	pgtemplate := templates.MustGet(packr.New("notificationsender-pg", "."), "notificationsender-pg.yaml")

	var ns namespace.Env
	if err := ctx.Get("namespace", &ns); err != nil {
		return nil, err
	}

	if err := ctx.Get("notificationsender", &p, NotificationHook); err != nil {
		return nil, err
	}

	var result []io.Reader
	if p.Database.URL == "" && p.Database.Local {
		p.Database.URL = "postgres://notificationsender:notificationsender@notificationsender-postgres/notificationsender?sslmode=disable"
		var a bytes.Buffer
		da := struct {
			namespace.Env
		}{
			ns,
		}
		err := pgtemplate.Execute(&a, da)
		if err != nil {
			return nil, errors.Wrap(err, "failed to render pg template")
		}

		result = append(result, &a)
	}

	if ctx.Common().MustTokenDRelease() >= types.TraefikCopTokenDRelease {
		if err := p.Cop.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate cop config")
		}
	} else {
		if err := p.Janus.Populate(p.Name()); err != nil {
			return nil, errors.Wrap(err, "failed to populate janus config")
		}
	}

	var b bytes.Buffer
	d := struct {
		types.Common
		namespace.Env
		NotificationSender Pipeline
	}{
		ctx.Common(),
		ns,
		p,
	}
	err := template.Execute(&b, d)
	if err != nil {
		return nil, errors.Wrap(err, "failed to render template")
	}

	result = append(result, &b)
	return result, nil
}

func (p Pipeline) Environment(ctx types.Context) (interface{}, error) {
	if err := ctx.Get("notificationsender", &p, NotificationHook); err != nil {
		return nil, errors.Wrap(err, "failed to get env")
	}

	return p, nil
}
