package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/davecgh/go-spew/spew"
	validation "github.com/go-ozzo/ozzo-validation"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/pmezard/go-difflib/difflib"
	"gitlab.com/distributed_lab/logan/v3/errors"
	genconfig "gitlab.com/tokend/tokend-cli"
	"gitlab.com/tokend/tokend-cli/internal/sentry"
	"gitlab.com/tokend/tokend-cli/types"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

func main() {
	context := genconfig.NewDefaultContext()

	var (
		// check following issues to see if kingpin supports `-f -` already,
		// meanwhile no stdin for ya
		// https://github.com/alecthomas/kingpin/issues/90
		// https://github.com/alecthomas/kingpin/issues/65
		app = kingpin.New("genconfig", "generating environments configs and producing k8s deployments for them")
		// root commands:
		gen         = app.Command("generate", "generate stuff").Alias("gen")
		validate    = app.Command("validate", "")
		pipelines   = app.Command("pipelines", "print available pipelines")
		apply       = app.Command("apply", "finds diff between local and remote state, applies to remote")
		applyInFile = apply.Flag("file", "environment configuration file").Short('f').Required().String()
		// generated environment
		genEnv        = gen.Command("environment", "generate environment config").Alias("env")
		genEnvInFile  = genEnv.Flag("file", "initial configuration file").Short('f').Required().String()
		genEnvOutFile = genEnv.Flag("output", "generated configuration file").Short('o').Required().String()
		// validate environment
		validateEnv       = validate.Command("environment", "").Alias("env")
		validateEnvInFile = validateEnv.Flag("file", "environment configuration file").Short('f').Required().String()
		// generate k8s
		genK8S        = gen.Command("k8s", "generate K8S deployment from environment")
		genK8SInFile  = genK8S.Flag("file", "environment configuration file").Short('f').Required().String()
		genK8SOutFile = genK8S.Flag("output", "Kubernetes deployment file").Short('o').Required().String()
		// create cluster by kops
		//createCluster = create.Command("create", "create kops kluster")
		//createClusterStage = createC.Flag("stage", "create stage config").Short('s')
		//createClusterProduction = createC.Flag("prod", "create prod config").Short('p')
		//createClusterNameProduct = createC.Flag("name", "name you product").Short('n').Required().String()
		//createClusterRegionProduct = createC.Flag("region", "region you product aws").Short('n').Required().String()
	)

	sentryFlags(genEnv)
	kuberFlags(apply)
	//createFlags(createC)

	cmd, err := app.Parse(os.Args[1:])
	if err != nil {
		log.Fatal(err)
	}

	switch cmd {
	case validateEnv.FullCommand():
		rc, err := readcloser(validateEnvInFile)
		if err != nil {
			log.Fatalf(err.Error())
		}
		defer rc.Close()

		if err := context.Decode(rc); err != nil {
			log.Fatalf("failed to load env: %v", err)
		}

		if err := context.Validate(); err != nil {
			printValidationError(err)
		}
	case pipelines.FullCommand():
		fmt.Println("available pipelines:")
		for _, pipeline := range context.AvailablePipelines() {
			fmt.Println("\t", pipeline.Name())
		}
		//case createCluster.FullCommand():
		//fmt.Println("Start create you cluster")
		//if createClusterNameProduct == nil {
		//	fmt.Println("You not set cluster name")
		//}
		//if createClusterRegionProduct == nil {
		//	fmt.Println("You not set region you cluster")
		//}

	case genK8S.FullCommand():
		rc, err := readcloser(genK8SInFile)
		if err != nil {
			log.Fatalf(err.Error())
		}
		defer rc.Close()

		wc, err := writecloser(genK8SOutFile)
		if err != nil {
			log.Fatalf(err.Error())
		}
		defer wc.Close()

		if err := context.Decode(rc); err != nil {
			log.Fatalf("failed to load env: %v", err)
		}

		if err := context.Validate(); err != nil {
			printValidationError(err)
		}

		r, err := context.GenerateDeployment()
		if err != nil {
			log.Fatalf("failed generate deployment: %v", err)
		}

		if _, err := io.Copy(wc, r); err != nil {
			log.Fatalf("faile to write output: %v", err)
		}
	case genEnv.FullCommand():
		rc, err := readcloser(genEnvInFile)
		if err != nil {
			log.Fatalf(err.Error())
		}
		defer rc.Close()

		wc, err := writecloser(genEnvOutFile)
		if err != nil {
			log.Fatalf(err.Error())
		}
		defer wc.Close()

		if err := context.Decode(rc); err != nil {
			log.Fatalf("failed to load env: %v", err)
		}

		if err := context.Validate(); err != nil {
			printValidationError(err)
		}

		context.SetSentry(sentryClient())

		if err := context.GenerateEnvironment(); err != nil {
			log.Fatalf("failed to generate environment: %v", err)
		}

		if err := context.Encode(wc); err != nil {
			log.Fatalf("failed to encode env: %v", err)
		}
	case apply.FullCommand():
		kube, err := kubeClient()
		if err != nil {
			log.Fatalf("faile to get kuber client: %v", err)
		}

		context.SetKube(kube)

		rc, err := readcloser(applyInFile)
		if err != nil {
			log.Fatalf(err.Error())
		}
		defer rc.Close()

		if err := context.Decode(rc); err != nil {
			log.Fatalf("failed to load env: %v", err)
		}

		if err := context.Validate(); err != nil {
			printValidationError(err)
		}

		remoteEnv, err := context.RemoteEnv()
		if err != nil {
			log.Fatalf("failed to get remote env: %v", err)
		}

		if remoteEnv == nil {
			log.Fatalf("TODO: looks like you have not deployed yet flow")
		}

		diff, err := getDiff(context.LocalEnv(), remoteEnv)
		if err != nil {
			log.Fatalf("failed to compute diff: %v", err)
		}

		if diff != "" {
			ok, err := prompt(os.Stdin, "Would you like to continue deploy?")
			if err != nil {
				log.Fatalf("prompt failed: %v", err)
			}
			if !ok {
				return
			}
		}

		r, err := context.GenerateDeployment()
		if err != nil {
			log.Fatalf("failed generate deployment: %v", err)
		}

		if err := kuberApply(r); err != nil {
			log.Fatalf("failed to apply env configuration: %v", err)
		}
	}
}

//
// where-should-we-put-those utils?
//

func getDiff(local, remote *types.Env) (string, error) {
	// Dumping local and remote states
	spewConfig := spew.ConfigState{
		Indent:                  " ",
		DisablePointerAddresses: true,
		DisableCapacities:       true,
		SortKeys:                true,
		// Will sort maps' keys
		SpewKeys: true,
	}
	localDumped := spewConfig.Sdump(*local)
	remoteDumped := spewConfig.Sdump(*remote)

	diff := difflib.UnifiedDiff{
		A:        difflib.SplitLines(localDumped),
		B:        difflib.SplitLines(remoteDumped),
		FromFile: "Local",
		ToFile:   "Remote",
	}
	return difflib.GetUnifiedDiffString(diff)
}

func kuberApply(r io.Reader) error {
	log.Println("Deploying")

	tmpfile, err := ioutil.TempFile("", "")
	if err != nil {
		return errors.Wrap(err, "failed to create temp file")
	}

	defer os.Remove(tmpfile.Name())

	_, err = io.Copy(tmpfile, r)
	if err != nil {
		return errors.Wrap(err, "failed to write tempfile")
	}

	cmd := exec.Command("kubectl", "apply", "-f", tmpfile.Name())

	// FIXME: not sure ignoring err is ok here
	stdoutReader, _ := cmd.StdoutPipe()
	stdoutScanner := bufio.NewScanner(stdoutReader)
	go func() {
		for stdoutScanner.Scan() {
			fmt.Println(stdoutScanner.Text())
		}
	}()
	stderrReader, _ := cmd.StderrPipe()
	stderrScanner := bufio.NewScanner(stderrReader)
	go func() {
		for stderrScanner.Scan() {
			fmt.Fprintln(os.Stderr, stderrScanner.Text())
		}
	}()

	err = cmd.Start()
	if err != nil {
		return errors.Wrap(err, "kubectl apply failed")
	}
	err = cmd.Wait()
	if err != nil {
		return errors.Wrap(err, "kubectl apply failed")
	}

	return nil
}

//
// CLI utils
//

var (
	sentryEnabled  *bool
	sentryToken    *string
	sentryOrg      *string
	sentryEndpoint *string
)

func sentryFlags(cmd *kingpin.CmdClause) {
	sentryEnabled = cmd.Flag("sentry-enabled", "").Envar("SENTRY_ENABLED").Bool()
	sentryToken = cmd.Flag("sentry-token", "").Envar("SENTRY_TOKEN").String()
	sentryEndpoint = cmd.
		Flag("sentry-endpoint", "").
		Envar("SENTRY_ENDPOINT").
		Default("https://sentry.io/api/0/").
		String()
	sentryOrg = cmd.Flag("sentry-org", "").Envar("SENTRY_ORG").String()
}

func sentryClient() *sentry.Client {
	if !(*sentryEnabled) {
		return nil
	}

	return sentry.NewClient(*sentryToken, *sentryEndpoint).
		WithOrganization(*sentryOrg)
}

var (
	kubeconfigFlag *string
	namespaceFlag  *string
)

func kuberFlags(cmd *kingpin.CmdClause) {
	defaultKubeconfig, err := homedir.Expand("~/.kube/config")
	if err != nil {
		log.Printf("failed to expand home directory: %v\n", err)
	}

	kubeconfigFlag = cmd.Flag("kubeconfig", "absolute path to the kubeconfig file").
		Default(defaultKubeconfig).
		Envar("KUBECONFIG").
		String()

	namespaceFlag = cmd.Flag("namespace", "namespace to deploy to").
		Default("default").
		Short('n').
		String()
}

func kubeClient() (*kubernetes.Clientset, error) {
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfigFlag)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build config")
	}
	return kubernetes.NewForConfig(config)
}

func kubeNamespace() string {
	return *namespaceFlag
}

func prompt(input io.Reader, message string) (bool, error) {
	reader := bufio.NewReader(input)

	fmt.Printf("%s [y/n]: ", message)

	response, err := reader.ReadString('\n')
	if err != nil {
		return false, errors.Wrap(err, "failed to read input")
	}

	response = strings.ToLower(strings.TrimSpace(response))
	return response == "y" || response == "yes", nil
}

func printValidationError(err error) {
	if err == nil {
		return
	}
	verr, ok := err.(validation.Errors)
	if ok {
		for k, v := range verr {
			fmt.Printf("%s: %v\n", k, v)
		}
	} else {
		log.Printf("validation failed: %v\n", err)
	}
	os.Exit(1)
}

func readcloser(fn *string) (rc io.ReadCloser, err error) {
	if fn == nil {
		panic(errors.Wrap(err, "non-nil filename expected"))
	}
	switch *fn {
	case "-":
		return nil, errors.New("stdin is not implemented")
		// rc = ioutil.NopCloser(os.Stdin)
	default:
		rc, err = os.Open(*fn)
		if err != nil {
			return nil, errors.Wrap(err, "failed to open file")
		}
	}
	return rc, nil
}

func writecloser(fn *string) (wc io.WriteCloser, err error) {
	if fn == nil {
		panic(errors.Wrap(err, "non-nil filename expected"))
	}
	switch *fn {
	case "-":
		return nil, errors.New("stdout is not implemented")
	default:
		wc, err = os.Create(*fn)
		if err != nil {
			return nil, errors.Wrap(err, "failed to open file")
		}
	}
	return wc, nil
}
