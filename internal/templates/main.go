package templates

import (
	"bytes"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"text/template"

	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/figure"

	"github.com/gobuffalo/packr/v2"
	"github.com/masterminds/sprig"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/tokend-cli/types"
)

//go:generate go-bindata -nometadata -ignore .+\.go$ -pkg templates -o bindata.go ./...
//go:generate gofmt -w bindata.go

func MustGet(box *packr.Box, name string) *template.Template {
	raw, err := box.Find(name)
	if err != nil {
		panic(errors.Wrap(err, "failed to load asset"))
	}
	tmpl, err := template.New(name).Funcs(sprig.TxtFuncMap()).Funcs(map[string]interface{}{
		"required":          Required,
		"publicKey":         PublicKey,
		"sentrysnippet":     SentrySnippet,
		"limitssnippet":     LimitsSnippet,
		"apigatewaysnippet": ApiGatewaySnippet,
		"copsnippet":        CopSnippet,
		"atoi_ge":           greaterEqualAsInts,
	}).Parse(string(raw))
	if err != nil {
		panic(errors.Wrap(err, "failed to parse template"))
	}
	return tmpl
}

func PublicKey(val interface{}) (interface{}, error) {
	str, ok := val.(string)
	if !ok {
		return nil, errors.New("expected string")
	}

	kp, err := keypair.ParseSeed(str)
	if err != nil {
		return nil, errors.Wrap(err, "expected private key")
	}

	return kp.Address(), nil

}

func Required(val interface{}) (interface{}, error) {
	if val == nil {
		return nil, fmt.Errorf("missing required value")
	}
	if str, ok := val.(string); ok && str == "" {
		return nil, fmt.Errorf("missing required value")
	}
	return val, nil
}

type SentryConfig struct {
	Disabled bool   `fig:"disabled"`
	DSN      string `fig:"dsn" yaml:",omitempty"`
}

func (s *SentryConfig) Populate(name string, ctx types.Context) error {
	if s.Disabled {
		return nil
	}

	if s.DSN == "" {
		dsn, err := ctx.SentryDSN(name)
		if err != nil {
			return errors.Wrap(err, "failed to get Sentry DSN")
		}
		s.DSN = dsn.Secret
	}

	return nil
}

func SentrySnippet(sentry SentryConfig) (string, error) {
	t, err := template.New("sentry-snippet").Funcs(sprig.TxtFuncMap()).Funcs(map[string]interface{}{
		"required":      Required,
		"publicKey":     PublicKey,
		"sentrysnippet": SentrySnippet,
	}).Parse(`
log:
  level: info
  disable_sentry: {{ .Sentry.Disabled }}
{{ if not .Sentry.Disabled }}
sentry:
  level: warn
  dsn: {{ required .Sentry.DSN }}
{{ end }}
`)
	if err != nil {
		panic(errors.Wrap(err, "failed to parse sentry-snippet template"))
	}
	var b bytes.Buffer
	err = t.Execute(&b, struct {
		Sentry SentryConfig
	}{
		sentry,
	})
	if err != nil {
		return "", errors.Wrap(err, "failed to render sentry-snippet")
	}
	return b.String(), nil
}

type LimitsConfig struct {
	Resources struct {
		Limits struct {
			CPU    string `fig:"cpu" yaml:",omitempty"`
			Memory string `fig:"memory" yaml:",omitempty"`
		} `fig:"limits" yaml:",omitempty"`
		Requests struct {
			CPU    string `fig:"cpu" yaml:",omitempty"`
			Memory string `fig:"memory" yaml:",omitempty"`
		} `fig:"requests" yaml:",omitempty"`
	} `fig:"resources" yaml:",omitempty"`
}

func LimitsSnippet(limits LimitsConfig) (string, error) {

	t, err := template.New("limits-snippet").Funcs(sprig.TxtFuncMap()).Funcs(map[string]interface{}{
		"limitssnippet": LimitsSnippet,
	}).Parse(`
resources:
  limits:
    cpu: "{{ .Limits.Resources.Limits.CPU }}"
    memory: "{{ .Limits.Resources.Limits.Memory }}"
  requests:
    cpu: "{{ .Limits.Resources.Requests.CPU }}"
    memory: "{{ .Limits.Resources.Requests.Memory }}"
`)
	if err != nil {
		panic(errors.Wrap(err, "failed to parse limits-snippet template"))
	}
	var b bytes.Buffer
	err = t.Execute(&b, struct {
		Limits LimitsConfig
	}{
		limits,
	})
	if err != nil {
		return "", errors.Wrap(err, "failed to render limits-snippet")
	}
	return b.String(), nil
}

type JanusConfig struct {
	Endpoint string `fig:"endpoint,required"`
	Upstream string `fig:"upstream,required"`
	Surname  string `fig:"surname"`
}

func (c *JanusConfig) Populate(name string) error {
	c.Endpoint = "http://janus"
	c.Upstream = fmt.Sprintf("http://%s", name)
	c.Surname = name
	return nil
}

type CopConfig struct {
	Endpoint      string `fig:"endpoint,required"`
	Upstream      string `fig:"upstream,required"`
	ServiceName   string `fig:"servicename,required"`
	ServicePort   string `fig:"serviceport"`
	ServicePrefix string `fig:"serviceprefix" yaml:",omitempty"`
}

var CopPrefixHook = figure.Hooks{
	"templates.CopConfig": func(value interface{}) (reflect.Value, error) {
		raw := cast.ToStringMap(value)

		var cfg struct {
			Prefix string `fig:"serviceprefix"`
		}

		if err := figure.Out(&cfg).From(raw).Please(); err != nil {
			return reflect.Value{}, nil
		}

		return reflect.ValueOf(CopConfig{ServicePrefix: cfg.Prefix}), nil
	},
}

func (c *CopConfig) Populate(name string) error {
	c.Endpoint = "http://cop"
	c.Upstream = fmt.Sprintf("http://%s", name)
	c.ServiceName = fmt.Sprintf("%s-service", name)
	c.ServicePort = "80"

	// since errhandler, api and horizon are explicitly passing sets of rules when registering themselves,
	// they does not need prefixes, so explicitly omitting them here
	needsPrefix := !strings.HasPrefix(name, "api") && !strings.HasPrefix(name, "horizon") &&
		!strings.HasPrefix(name, "errhandler")

	if c.ServicePrefix == "" && needsPrefix {
		c.ServicePrefix = fmt.Sprintf("/integrations/%s", name)
	}

	return nil
}

func ApiGatewaySnippet(tokendRelease string, cop CopConfig, janus JanusConfig) (string, error) {
	t, err := template.New("api-gateway-snippet").Funcs(sprig.TxtFuncMap()).Funcs(map[string]interface{}{
		"apigatewaysnippet": ApiGatewaySnippet,
	}).Parse(fmt.Sprintf(`
{{ if ge .TokenDRelease "%d" }}
cop:
  endpoint: {{ .Cop.Endpoint }}
  upstream: {{ .Cop.Upstream }}
  service_name: {{ .Cop.ServiceName }}
  service_port: {{ .Cop.ServicePort }}
{{ if ne .Cop.ServicePrefix "" }}
  service_prefix: {{ .Cop.ServicePrefix }}
{{ end }}
{{ else }}
janus:
  endpoint: {{ .Janus.Endpoint }}
  upstream: {{ .Janus.Upstream }}
  surname:  {{ .Janus.Surname }}
{{ end }}
`, types.TraefikCopTokenDRelease))
	if err != nil {
		panic(errors.Wrap(err, "failed to parse api-gateway-snippet template"))
	}

	var b bytes.Buffer
	err = t.Execute(&b, struct {
		TokenDRelease string
		Cop           CopConfig
		Janus         JanusConfig
	}{
		tokendRelease,
		cop,
		janus,
	})
	if err != nil {
		return "", errors.Wrap(err, "failed to render api-gateway-snippet")
	}

	return b.String(), nil
}

func CopSnippet(tokendRelease string, config CopConfig) (string, error) {
	release, err := strconv.Atoi(tokendRelease)
	if err != nil {
		return "", errors.Wrap(err, "cannot parse TokenDRelease")
	}

	if release < types.TraefikCopTokenDRelease {
		return "", errors.Errorf("trying to use Cop integration with TokenDRelease version less than %d, aborting", types.TraefikCopTokenDRelease)
	}

	return ApiGatewaySnippet(tokendRelease, config, JanusConfig{})
}

func greaterEqualAsInts(a, b string) (bool, error) {
	switch a {
	case "pre3":
		return b == "pre3", nil
	default:
		intA, err := strconv.Atoi(a)
		if err != nil {
			return false, errors.Wrap(err, "failed to convert first arg to int")
		}
		intB, err := strconv.Atoi(b)
		if err != nil {
			return false, errors.Wrap(err, "failed to convert second arg to int")
		}
		return intA >= intB, nil
	}
}
