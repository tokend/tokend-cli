package genconfig

import (
	"context"
	"fmt"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/api"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/metrics"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/csvparser"

	"io"
	"io/ioutil"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/withdrawcloser"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/masspayments"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/identitystorage"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/opsender"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/mailgunnotificator"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/firebasenotificator"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/twilionotificator"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/d7notificator"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/notificationsrouter"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/errhandler"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/cop"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/smartcontracts"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/traefik"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/escrow"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/booking"

	"gitlab.com/tokend/tokend-cli/internal/pipelines/scheduler"

	assetremover "gitlab.com/tokend/tokend-cli/internal/pipelines/asset-remover"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/locator"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/marketplace"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/sponsor"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/tokend-cli/internal/lorem"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/adks"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/charts"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/coinpayments"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/core"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/csvsvc"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/dns"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/erc20"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/forbill"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/horizon"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/idmind"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/ingress"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/invites"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/namespace"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/notificationsender"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/notificator"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/paymentsnotificator"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/pollcloser"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/pre3modules"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/proxypayments"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/redis"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/salecloser"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/stellar"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/templatesprovider"
	"gitlab.com/tokend/tokend-cli/internal/pipelines/webclients"
	"gitlab.com/tokend/tokend-cli/internal/sentry"
	"gitlab.com/tokend/tokend-cli/types"
	yaml "gopkg.in/yaml.v2"
	kubeerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type Context struct {
	availablePipelines   []types.Pipeline
	pipelines            map[string]types.Pipeline
	env                  types.Env
	names                <-chan string
	rawEnvironmentConfig []byte

	kube   *kubernetes.Clientset
	sentry *sentry.Client
}

func NewContext() types.Context {
	return &Context{
		availablePipelines: []types.Pipeline{},
		pipelines:          map[string]types.Pipeline{},
		names:              lorem.RandomNameIter(),
	}
}

func NewDefaultContext() types.Context {
	context := NewContext()

	context.RegisterPipeline(&namespace.Pipeline{})
	context.RegisterPipeline(&cop.Pipeline{})
	context.RegisterPipeline(&traefik.Pipeline{})
	context.RegisterPipeline(&core.Pipeline{})
	context.RegisterPipeline(&horizon.Pipeline{})
	context.RegisterPipeline(&adks.Pipeline{})
    context.RegisterPipeline(&api.Pipeline{})
	context.RegisterPipeline(&webclients.Pipeline{})
	context.RegisterPipeline(&salecloser.Pipeline{})
	context.RegisterPipeline(&ingress.Pipeline{})
	context.RegisterPipeline(&charts.Pipeline{})
	context.RegisterPipeline(&coinpayments.Pipeline{})
	context.RegisterPipeline(&notificator.Pipeline{})
	context.RegisterPipeline(&pre3modules.Pipeline{})
	context.RegisterPipeline(&templatesprovider.Pipeline{})
	context.RegisterPipeline(&idmind.Pipeline{})
	context.RegisterPipeline(&pollcloser.Pipeline{})
	context.RegisterPipeline(&notificationsender.Pipeline{})
	context.RegisterPipeline(&dns.Pipeline{})
	context.RegisterPipeline(&invites.Pipeline{})
	context.RegisterPipeline(&forbill.Pipeline{})
	context.RegisterPipeline(&proxypayments.Pipeline{})
	context.RegisterPipeline(&paymentsnotificator.Pipeline{})
	context.RegisterPipeline(&marketplace.Pipeline{})
	context.RegisterPipeline(&sponsor.Pipeline{})
	context.RegisterPipeline(&locator.Pipeline{})
	context.RegisterPipeline(&assetremover.Pipeline{})
	context.RegisterPipeline(&csvsvc.Pipeline{})
	context.RegisterPipeline(&stellar.Pipeline{})
	context.RegisterPipeline(&redis.Pipeline{})
	context.RegisterPipeline(&erc20.Pipeline{})
	context.RegisterPipeline(&scheduler.Pipeline{})
	context.RegisterPipeline(&booking.Pipeline{})
	context.RegisterPipeline(&escrow.Pipeline{})
	context.RegisterPipeline(&errhandler.Pipeline{})
	context.RegisterPipeline(&smartcontracts.Pipeline{})
	context.RegisterPipeline(&notificationsrouter.Pipeline{})
	context.RegisterPipeline(&twilionotificator.Pipeline{})
	context.RegisterPipeline(&d7notificator.Pipeline{})
	context.RegisterPipeline(&firebasenotificator.Pipeline{})
	context.RegisterPipeline(&mailgunnotificator.Pipeline{})
	context.RegisterPipeline(&opsender.Pipeline{})
	context.RegisterPipeline(&identitystorage.Pipeline{})
	context.RegisterPipeline(&masspayments.Pipeline{})
	context.RegisterPipeline(&withdrawcloser.Pipeline{})
	context.RegisterPipeline(&csvparser.Pipeline{})
	context.RegisterPipeline(&metrics.Pipeline{})

	return context
}

func (c *Context) AvailablePipelines() []types.Pipeline {
	return c.availablePipelines
}

func (c *Context) Decode(r io.Reader) error {
	raw, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}
	c.rawEnvironmentConfig = raw
	if err := yaml.Unmarshal(raw, &c.env); err != nil {
		return err
	}
	return nil
}

func (c *Context) Encode(w io.Writer) error {
	if err := yaml.NewEncoder(w).Encode(&c.env); err != nil {
		return err
	}
	return nil
}

func (c Context) GenerateEnvironment() error {
	for _, pipeline := range c.AvailablePipelines() {
		// check if there is config for particular pipeline
		if _, ok := c.env.Pipelines[pipeline.Name()]; !ok {
			// skipping, since pipeline is not configured
			continue
		}
		pipeEnv, err := pipeline.Environment(&c)
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("failed to generate env for %s", pipeline.Name()))
		}
		if err := c.Set(pipeline.Name(), pipeEnv); err != nil {
			return errors.Wrap(err, fmt.Sprintf("failed to set pipeline data: %s", pipeline.Name()))
		}
	}
	return nil
}

func (c Context) GenerateDeployment() (io.Reader, error) {
	var results []io.Reader
	for _, pipeline := range c.AvailablePipelines() {
		// check if there is config for particular pipeline
		if _, ok := c.env.Pipelines[pipeline.Name()]; !ok {
			// skipping, since pipeline is not configured
			continue
		}
		result, err := pipeline.Deployment(&c)
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate pipeline", logan.F{
				"pipeline": pipeline.Name(),
			})
		}
		results = append(results, result...)
	}
	return io.MultiReader(results...), nil
}

func (c Context) RawEnvironmentConfig() []byte {
	return c.rawEnvironmentConfig
}

func (c Context) Get(name string, dest interface{}, hooks ...figure.Hooks) error {
	raw, ok := c.env.Pipelines[name]
	if !ok {
		// FIXME required, but at the same time extremely tricky
		return nil
	}

	//fmt.Fprintf(os.Stderr, "FOOBAR %T\n", raw)

	m, err := cast.ToStringMapE(raw)
	if err != nil {
		return err
	}
	hooks = append(hooks, figure.BaseHooks)
	if err := figure.Out(dest).With(hooks...).From(m).Please(); err != nil {
		return err
	}
	return nil
}

func (c Context) Set(name string, val interface{}) error {
	// FIXME disgusting
	raw, err := yaml.Marshal(val)
	if err != nil {
		return err
	}
	m := map[string]interface{}{}
	if err := yaml.Unmarshal(raw, m); err != nil {
		return err
	}
	c.env.Pipelines[name] = m
	return nil
}

func (c Context) Common() types.Common {
	return c.env.Common
}

type VersionedPipeline interface {
	CurrentVersion() string
}

func (c Context) Validate() error {
	result := validation.Errors{}

	// validate component versions
	for _, pipeline := range c.AvailablePipelines() {
		versioned, ok := pipeline.(VersionedPipeline)
		if !ok {
			continue
		}

		var probe struct {
			Version string
		}
		if err := c.Get(pipeline.Name(), &probe); err != nil {
			return errors.Wrap(err, "failed to get pipeline env")
		}

		if probe.Version == "" {
			probe.Version = "1"
		}

		if probe.Version != versioned.CurrentVersion() {
			result[pipeline.Name()] = fmt.Errorf("expected version %s got %s", versioned.CurrentVersion(), probe.Version)
		}
	}

	return result.Filter()
}

func (c *Context) RegisterPipeline(pipeline types.Pipeline) {
	if _, ok := c.pipelines[pipeline.Name()]; ok {
		panic(fmt.Errorf("pipeline already registered: %s", pipeline.Name()))
	}
	c.pipelines[pipeline.Name()] = pipeline
	c.availablePipelines = append(c.AvailablePipelines(), pipeline)
}

func (c Context) RandomString() string {
	return <-c.names
}

func (c *Context) MustNamespace() string {
	var ns struct {
		Namespace string
	}

	if err := c.Get("namespace", &ns); err != nil {
		panic(errors.Wrap(err, "failed to decode namespace"))
	}

	return ns.Namespace
}

func (c *Context) SetKube(kube *kubernetes.Clientset) {
	c.kube = kube
}

func (c *Context) LocalEnv() *types.Env {
	return &c.env
}

func (c *Context) RemoteEnv() (*types.Env, error) {
	namespace := c.MustNamespace()

	// Load "genconfig" configmap
	configMap, err := c.kube.CoreV1().
		ConfigMaps(namespace).
		Get(context.Background(), "genconfig", metav1.GetOptions{})
	if err != nil {
		// If there is no "genconfig" then, probably nobody deployed yet
		if statusErr, ok := err.(*kubeerrors.StatusError); ok {
			if statusErr.Status().Code == 404 {
				return nil, nil
			}
		}
		return nil, errors.Wrap(err, "failed to fetch configmap")
	}

	// Unmarshal env.yaml field value, which is our config
	var remoteEnv types.Env
	err = yaml.Unmarshal([]byte(configMap.Data["env.yaml"]), &remoteEnv)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal configmap")
	}
	return &remoteEnv, nil
}

func (c *Context) SetSentry(client *sentry.Client) {
	c.sentry = client
}

func (c *Context) SentryDSN(component string) (sentry.Key, error) {
	if c.sentry == nil {
		return sentry.Key{}, errors.New("Sentry client is not initilized")
	}

	teamSlug, err := c.sentry.EnsureTeam(c.Common().Name)
	if err != nil {
		return sentry.Key{}, errors.Wrap(err, "failed to ensure Sentry team")
	}

	teamName := c.Common().Name
	sentryClient := c.sentry.WithTeam(&teamName, teamSlug)

	if err := sentryClient.EnsureProject(component); err != nil {
		return sentry.Key{}, errors.Wrap(err, "failed to ensure Sentry project")
	}

	key, err := sentryClient.ClientKey(component, "tokendctl")
	if err != nil {
		return sentry.Key{}, errors.Wrap(err, "failed to retrive Sentry key")
	}

	return key, nil
}
