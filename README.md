# tokendctl

# Deployment Guide

> NOTE: Currently only deployment to Kubernetes cluster on AWS is supported out of the box.

## TL;DR

These instructions are a guideline for what you should generally do.

While just copy-pasting following commands should (probably) get you up and running
it's highly adviced to get yourself familiar with tools and options used below
to get the idea what's happening under the hood.

Kubernetes part:

```sh
# to make sure kops will load your ~/.aws/config
export AWS_SDK_LOAD_CONFIG=1
# tell tools below which profile they should use to authenticate with AWS
export AWS_PROFILE=default
# k8s cluster name
NAME=example
# AWS region to deploy to
REGION=us-east-2

# create S3 bucket for storing kops state
aws s3 mb s3://$NAME-kops-state --region $REGION

# spin up k8s cluster
kops create cluster \
--name $NAME.k8s.local \
--zones=${REGION}a \
--master-zones=${REGION}a \
--networking kube-router \
--dns=private \
--topology=private \
--node-count=4 \
--node-size=c5.large \
--master-size=t3.medium --master-count=1 \
--ssh-public-key ~/.ssh/id_rsa.pub \
--state s3://$NAME-kops-state \
--cloud=aws \
--yes

# make sure everything provisioned correctly
kops validate cluster --state s3://$NAME-kops-state

# allow cluster instances to access S3
aws iam attach-role-policy --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess --role-name nodes.$NAME.k8s.local
```

TokenD part:

> it's important that at this point `example.env.yaml` contains correct required values, comments there should get you an idea of what's going on

```sh
# populate environment config
tokendctl gen env -f example.env.yaml -o $NAME.env.yaml

# generate k8s deployment
tokendctl gen k8s -f $NAME.env.yaml -o $NAME.k8s.yaml

# deploy TokenD resources to the k8s cluster
kubectl apply -f $NAME.k8s.yaml
```

## Prerequisites

* Installed and configured [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)

* Running Kubernetes cluster on AWS. We recommend [`kops`](https://github.com/kubernetes/kops) for managing Kubernetes side of the things, [here](https://kubernetes.io/docs/setup/pick-right-solution/) might be a good place to start looking for alternatives.

* While it's possible to spin up Postgres instances in cluster it is recommended to use more robust solutions like
  [RDS](https://console.aws.amazon.com/rds/home) or any other managed database offering.

* ACM certiificate for domains used in deployment


## Hardware Recommendations

The recommended environment for running a production-ready deployment of TokenD on AWS with Kubernetes is as follows.

|Resources|Recommended parameters|Description|
|:-------:|:---------------------:|:--------:|
|Computing Instances|2+ master instances ([t3.medium](https://aws.amazon.com/ec2/instance-types/t3/) or higher) 4+ worker nodes ([с5.large](https://aws.amazon.com/ec2/instance-types/с5/) or higher)|Combined with a multi-AZ/regional placement of cluster instances allows running a high-availability TokenD deployment|
|Ingress Load Balancer|ELB or similar|Allows achieving fault tolerance for distributed deployment, ensuring performance and scalability|
|Managed PostgreSQL Database|RDS on a [c5.large](https://aws.amazon.com/ec2/instance-types/c5/) instance, or similar|Reliable storage of users’ wallets and identity data|
|Object Storage|S3 or other compatible offerings|Object storage is used for backing up blockchain history, KYC data, etc.|

Storage requirements highly depends on the use case and activity of the users. Default configuration will use EBS backed VPCs of 128 GB and add additional storage when needed.

## Some notable features

* IP whitelisting
* Original visitor IP restoration
* Cloudflare [setup](cloudflare.md)

## Generating environment config

`example.env.yaml` contains template for generating TokenD environment config. After modifying it to your hearts content
you could use generation step to populate some fields automagically.

```sh
$ tokendctl gen env -f example.env.yaml -o demo.env.yaml
```

Now `demo.env.yaml` should contain full deployment configuration which you will use for deployment.
Make sure it's stored securely.

## Audit node

It is possible to add public audit node. It will be in consensus with all the main private nodes.
Bucket with history will be available by address `audithistorydomain`(see env file).
S3 bucket with audit node history must be made public.
It can be done setting following bucket policy:
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowPublicRead",
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::<bucket_name>/*"
        }
    ]
}
```

To support peer connections, separate load balancer of type "nlb" (network load balancer) will be created.
To bind pretty name to public node load balancer another DNS record must be created:
```
<public_domain> CNAME <audit_node_elb>
```


# Post-deployment steps

## Initial system configuration

TokenD provides flexible configuration options and included modules make certain
assumptions about system state.

Recommended way to maintain configuration is [TokenD Terraform provider](https://github.com/tokend/terraform-provider-tokend)

Following Terraform configuration should get you up and running:

```terraform
provider "tokend" {
  # `pipelines.core.masteraccountid`
  account  = "GBA4EX43M25UPV4WIE6RRMQOFTWXZZRIPFAI5VPY6Z2ZVVXVWZ6NEOOB"
  # master signer allowed to perform required operations
  signer   = "SAMJKTZVW5UOHCDK5INYJNORF2HRKYI72M5XSZCBYAHQHR34FFR4Z6G4"
  # `pipelines.ingress.apidomain`
  endpoint = "https://api.demo.tokend.io"
}

module "tokend_vanilla" {
  source = "git::https://github.com/tokend/terraform-tokend-vanilla.git?ref=v1.0.0"
}
```

It uses [module](https://github.com/tokend/terraform-tokend-vanilla) which you
could fork or use a reference implementing your own configuration.

## Setting up audit public node

It is possible to add audit node to deployment. Audit node will have its own load balancer, 
so separate DNS CNAME record have to be set manually. 
Then, peers could connect to public node by address `pipelines.ingress.publicnodedomain:8090`.

S3 bucket with history of audit node have to be made public manually. Then, history would be publicly accessible on `pipelines.ingress.historydomain`.

# Removing your cluster

## Detach IAM policy
```
aws iam detach-role-policy --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess --role-name nodes.$NAME.k8s.local
```

## Delete cluster
```
kops delete cluster --name $NAME.k8s.local --state s3://$NAME-kops-state --yes
```