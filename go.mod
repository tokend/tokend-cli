module gitlab.com/tokend/tokend-cli

go 1.16

require (
	github.com/Masterminds/goutils v1.1.1 // indirect
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/atlassian/go-sentry-api v0.0.0-20210217035006-1aca435ce796
	github.com/davecgh/go-spew v1.1.1
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/gobuffalo/packr/v2 v2.8.1
	github.com/gosimple/slug v1.9.0
	github.com/huandu/xstrings v1.3.2 // indirect
	github.com/masterminds/sprig v2.22.0+incompatible
	github.com/mitchellh/copystructure v1.1.2 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/nullstyle/go-xdr v0.0.0-20180726165426-f4c839f75077 // indirect
	github.com/pkg/errors v0.9.1
	github.com/pmezard/go-difflib v1.0.0
	github.com/spf13/cast v1.3.1
	github.com/stretchr/testify v1.7.0 // indirect
	gitlab.com/distributed_lab/figure v2.1.0+incompatible
	gitlab.com/distributed_lab/logan v3.8.0+incompatible
	gitlab.com/tokend/go v3.14.0+incompatible
	gitlab.com/tokend/keypair v0.0.0-20190412110653-b9d7e0c8b312
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gopkg.in/yaml.v2 v2.4.0
	k8s.io/api v0.21.0
	k8s.io/apimachinery v0.21.0
	k8s.io/client-go v0.21.0
)
