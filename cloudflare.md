# Cloudflare

[Cloudflare](https://www.cloudflare.com/) can be setup using a combination of the following features:

* IP whitelisting
* Original visitor IP restoration

## DNS records

Cloudflare works by proxying requests through their servers, hiding server IPs and DNS records.

Wildcard records are being exposed and not being proxied using plans other that Enterprise. 
One way of overcoming this is to add separate CNAME records instead of one wildcard record.

Keep in mind that SSL certificate has to be issued for all of the subdomains that are planned to be used, e.g.
 `a.base.domain`, `b.base.domain` and not `*.base.domain`. 

Create CNAME records on cloudflare of the following form
* `base.domain` CNAME `<AWS load balancer address>`
* `a.base.domain` CNAME `base.domain`
* `b.base.domain` CNAME `base.domain` etc.

## IP whitelisting

To allow only requests through Cloudflare, the following has to be done:
* Get Cloudflare IP ranges [here](https://www.cloudflare.com/ips/)
* Set field `ipwhitelistrequired` to `true` in ingress section of env file. 
* Put IP ranges in  `ingress` section of env file:
```yaml
ingress:
  ...
  ipwhitelistrequired: true
  whitelistedips:
  ...
    - 173.245.48.0/20
    - 103.21.244.0/22
    - 2400:cb00::/32
  ...
```

## Original visitor IP restoration

To restore original IPs the following has to be done:

* Set field `restorerealips` to `true` in ingress section of env file.
* Set field `realipheader` to redirect header provided by proxy in ingress section of env file. 
Cloudflare by default stores real ip in `CF-Connecting-IP` header.

```yaml
ingress:
  ...
  restorerealips: true
  realipheader: CF-Connecting-IP
  ...
```