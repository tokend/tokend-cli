build: build_linux build_darwin build_windows

build_linux:
	GOOS=linux go build -o ./bin/tokendctl-linux internal/cmd/genconfig.go

build_darwin:
	GOOS=darwin go build -o ./bin/tokendctl-darwin internal/cmd/genconfig.go

build_windows:
	GOOS=windows go build -o ./bin/tokendctl-windows internal/cmd/genconfig.go

