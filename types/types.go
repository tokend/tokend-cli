package types

import (
	"io"
	"strconv"

	"gitlab.com/distributed_lab/figure"
	"gitlab.com/tokend/tokend-cli/internal/sentry"
	"k8s.io/client-go/kubernetes"
)

const (
	TraefikCopTokenDRelease = 5
)

type Context interface {
	Decode(r io.Reader) error
	Encode(w io.Writer) error
	GenerateEnvironment() error
	GenerateDeployment() (io.Reader, error)
	RawEnvironmentConfig() []byte
	Get(name string, dest interface{}, hooks ...figure.Hooks) error
	Set(name string, val interface{}) error
	Common() Common
	Validate() error
	RandomString() string
	MustNamespace() string
	SetKube(kube *kubernetes.Clientset)
	LocalEnv() *Env
	RemoteEnv() (*Env, error)
	SetSentry(client *sentry.Client)
	SentryDSN(component string) (sentry.Key, error)
	RegisterPipeline(pipeline Pipeline)
	AvailablePipelines() []Pipeline
}

type Common struct {
	Name          string
	UseCase       string `fig:"usecase" yaml:",omitempty"`
	TokenDRelease string `fig:"tokendrelease"`
}

func (c Common) MustTokenDRelease() int {
	switch c.TokenDRelease {
	case "pre3":
		return 2
	default:
		r, err := strconv.Atoi(c.TokenDRelease)
		if err != nil {
			panic(err)
		}
		return r
	}
}

type Env struct {
	Common    Common
	Pipelines map[string]interface{}
}

type Pipeline interface {
	Name() string
	Environment(ctx Context) (interface{}, error)
	Deployment(ctx Context) ([]io.Reader, error)
}
