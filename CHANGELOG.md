# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

* Configurable PVC size for Horizon
* -f/-o options for commands with input/output
* Config for making core's node public
* identitymind pipeline

### Changed

* Core maintenance is disabled
* Commands no longer support reading stdin and writing stdout

## 2

### Fixed

* CORS headers for DELETE and PATCH methods
* Context is now importable

### Changed

* Pipelines without configuration are skipped
* Pipelines are now using `text/template` package for templating
* API(Identity) config now version 2

### Added

* Pipeline for gitlab.com/tokend/templates-svc
* Pipeline for gitlab.com/distributed_lab/notificator-server
* Pipeline for gitlab.com/tokend/coinpayments
* Support for pre3 release
* Sentry support for most of pipelines
* Persistent history option for Horizon
* Pipeline versioning support
* `validate env` command
* Experimental `apply` command
