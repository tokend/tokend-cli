package kubehelpers

import (
	"fmt"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	clientcmdapi "k8s.io/client-go/tools/clientcmd/api"
)

func GetAllAvailableContexts(kubeconfigPath string) (map[string]*clientcmdapi.Context, error) {
	clientConfigLoadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
	clientConfigLoadingRules.ExplicitPath = kubeconfigPath

	conf, err := clientConfigLoadingRules.Load()
	if err != nil {
		return nil, err
	}

	return conf.Contexts, nil
}

func GetAllAvailableContextNames(kubeconfigPath string) ([]string, error) {
	contexts, err := GetAllAvailableContexts(kubeconfigPath)
	if err != nil {
		return nil, err
	}

	availableNames := make([]string, len(contexts))
	i := 0
	for context := range contexts {
		availableNames[i] = context
		i++
	}

	return availableNames, nil
}

func GetAllNamespacesForContext(kubeconfPath, contextName string) ([]corev1.Namespace, error) {
	config, err := BuildConfigFromFlagsWithContext(kubeconfPath, contextName)
	if err != nil {
		return nil, err
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	namespacesList, err := clientset.CoreV1().Namespaces().List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	return namespacesList.Items, nil
}

// see: https://github.com/kubernetes/client-go/issues/192#issuecomment-349564767
func BuildConfigFromFlagsWithContext(kubeconfigPath, contextName string) (*rest.Config, error) {
	return clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
		&clientcmd.ClientConfigLoadingRules{ExplicitPath: kubeconfigPath},
		&clientcmd.ConfigOverrides{
			CurrentContext: contextName,
		}).ClientConfig()
}

func FindContextByNamespace(kubeconfigPath, byNamespace string) (string, *clientcmdapi.Context, error) {
	contexts, err := GetAllAvailableContexts(kubeconfigPath)
	if err != nil {
		return "", nil, err
	}

	for contextName, context := range contexts {
		// TODO: Don't like this, because it will load the same config again and again
		namespaces, _ := GetAllNamespacesForContext(kubeconfigPath, contextName)
		for _, namespace := range namespaces {
			if namespace.Name == byNamespace {
				return contextName, context, nil
			}
		}
	}

	return "", nil, fmt.Errorf("Failed to find %s namespace in the config", byNamespace)
}

func GetServicesInNamespace(kubeconfigPath, namespace, serviceLabel string) ([]corev1.Container, error) {
	contextName, _, err := FindContextByNamespace(kubeconfigPath, namespace)
	if err != nil {
		return nil, err
	}

	config, err := BuildConfigFromFlagsWithContext(kubeconfigPath, contextName)
	if err != nil {
		return nil, err
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	if serviceLabel != "" {
		serviceLabel = fmt.Sprintf("service=%s", serviceLabel)
	}
	pods, err := clientset.CoreV1().Pods(namespace).
		List(metav1.ListOptions{LabelSelector: serviceLabel})
	if err != nil {
		return nil, err
	}

	var deployedContainers []corev1.Container
	for _, pod := range pods.Items {
		deployedContainers = append(deployedContainers, pod.Spec.Containers...)
	}

	return deployedContainers, nil
}
